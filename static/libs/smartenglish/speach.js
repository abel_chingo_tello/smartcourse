var speachonline=navigator.onLine?true:false;
//speachonline=false;
if(speachonline==true){
    $.ajax({
        async:true, 
        cache:false,      
        type: 'POST',   
        url: "https://abacoeducacion.org/web/static/media/imagenes/SE5.gif",
        success: function(data, textStatus, xhr) {
            if(xhr.status==200) speachonline=true;
            else speachonline=false;
        },
        complete: function(xhr, textStatus) {
            if(xhr.status==200) speachonline=true;
            else speachonline=false;
        },error:function(){
           // if(xhr.status==200) speachonline=true;
           // else speachonline=false;
        } 
      });
    }
//speachonline=false;
//variables globales 
var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var constraints = window.constraints = { audio: true, video: false};
navigator.getUserMedia = ( navigator.getUserMedia ||  navigator.webkitGetUserMedia ||  navigator.mozGetUserMedia ||  navigator.msGetUserMedia);
window.AudioContext = window.AudioContext || window.webkitAudioContext;
var speechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.oSpeechRecognition || window.msSpeechRecognition;
var mediaRecorder , recognizeronline;

//variables offline
var recognizer, recorder, callbackManager
var isRecorderReady = isRecognizerReady = false;
var wordList=[];
var grammarIds = [];
var grammars = [];
var farsesoffline=[];
var palabrasall='';
var _url_=_sysUrlBase_+'/static/libs/pocketsphinx.js/webapp/js';
function postRecognizerJob(message, callback){
    var msg = message || {};
    if (callbackManager) msg.callbackId = callbackManager.add(callback);
    if (recognizer) recognizer.postMessage(msg);
};
function spawnWorker(workerURL, onReady){
    recognizer = new Worker(workerURL);
    recognizer.onmessage = function(event) {
      onReady(recognizer);
    };
    recognizer.postMessage({'pocketsphinx.wasm': _url_+'/pocketsphinx.wasm', 'pocketsphinx.js': _url_+'/pocketsphinx.js'});
}
function updateUI() {if(isRecorderReady && isRecognizerReady) console.log('ya grabar offline')}
function updateStatus(newStatus) {console.log(newStatus);}
function displayRecording(display){
    if (display) console.log('grabando'); //document.getElementById('recording-indicator').innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    else console.log('no grabando');
};
var recognizerReady = function() {
    isRecognizerReady = true;
    updateUI();
    updateStatus("Recognizer ready");
};
var feedGrammar = function(g, index, id) {
    if (id && (grammarIds.length > 0)) grammarIds[0].id = id.id;
    if (index < g.length) {
      grammarIds.unshift({title: g[index].title});
      postRecognizerJob({command: 'addGrammar', data: g[index].g},function(id) {feedGrammar(grammars, index + 1, {id:id});});
    }else{ recognizerReady();}
};
var feedWords = function(words) {postRecognizerJob({command: 'addWords', data: words},function() {feedGrammar(grammars, 0);});};
var initRecognizer = function() {// You can pass parameters to the recognizer, such as : {command: 'initialize', data: [["-hmm", "my_model"], ["-fwdflat", "no"]]}
    postRecognizerJob({command: 'initialize', data: [["-kws_threshold", "1e-35"]]},function() { if (recorder) recorder.consumers = [recognizer];feedWords(wordList);});
};
var pnloffline='';
if(!speachonline){
window.onload = function(){
    callbackManager = new CallbackManager();    
    spawnWorker(_url_+"/recognizer.js", function(worker) {// This is the onmessage function, once the worker is fully loaded            
        worker.onmessage = function(e) { // This is the case when we have a callback id to be called
            if (e.data.hasOwnProperty('id')) {
                var clb = callbackManager.get(e.data['id']);
                var data = {};
                if ( e.data.hasOwnProperty('data')) data = e.data.data;
                if(clb) clb(data);
            }
            if (e.data.hasOwnProperty('hyp')) {// This is a case when the recognizer has a new hypothesis
                var newHyp = e.data.hyp;
                if (e.data.hasOwnProperty('final') &&  e.data.final) newHyp = newHyp;
                if(pnloffline.length){
                    var npaldoc=pnloffline.find('.txtalu1').text().split(' ').length;
                    var npalalum=newHyp.split(' ').length;
                    pnloffline.find('.edittexto_ch.alumno').removeClass('hide');
                    if(npalalum <= npaldoc) pnloffline.find('.txtalu2').text(newHyp);
                }
            }
            if (e.data.hasOwnProperty('status') && (e.data.status == "error")) { // This is the case when we have an error
                console.log("Error in " + e.data.command + " with code " , e.data.code);
            }
        };
        postRecognizerJob({command: 'lazyLoad',data: {folders: [], files: [["/", "kws.txt", "../kws.txt"],["/", "kws.dict", "../kws.dict"]]}}, initRecognizer);
    });
    audioContext = new AudioContext();
}}

var crearcontenido_speach=function(datos){
    var tmphtml='';
    var idtmp='pnl_'+datos.pnlidgui+'_'+Date.now();
    var pnl=$('#'+datos.pnlidgui+'.pnl-speach.docente');    
    crearonda(datos.ruta,pnl);
     pnl.find('.btnPlaytexto').removeClass('hidden').show();
    //console.log(datos);

    var iedit='<i class="fa fa-pencil"></i>';
    if(datos.tipo=='record'){
        iedit='<i class="fa fa-microphone-slash"></i>';
        tmphtml+='<div class="col-md-12 contentpregunta_speach" id="'+idtmp+'"><audio src="'+datos.ruta+'" class="" controls="true"></audio>';
    }else if(datos.tipo=='audio'){
        tmphtml+='<div class="col-md-12 contentpregunta_speach" id="'+idtmp+'"><audio src="'+datos.ruta+'" class="" controls="true"></audio>';
    }else if(datos.tipo=='video'){
        tmphtml+='<div class="col-md-12 contentpregunta_speach embed-responsive embed-responsive-16by9" id="'+idtmp+'"><video src="'+datos.ruta+'" class=" embed-responsive-item" controls="true"></video>';
    }else if(datos.tipo=='image'){
        tmphtml+='<div class="col-md-12 text-center contentpregunta_speach" id="'+idtmp+'"><img src="'+datos.ruta+'" class="img-responsive img-thumbnail" style="max-width:400px; max-height:400px;" />';
    }else if(datos.tipo=='texto'){
        tmphtml+='<div class="col-md-12 text-text-justify contentpregunta_speach" id="'+idtmp+'"><span class="texto">'+datos.ruta+'</span>';
    }     
    tmphtml+='<span class="btnacciones nopreview"><a href="#" class="btn btn-xs btn-warning btneditarpnl_speach" data-tipo="'+datos.tipo+'" data-pnlidgui="'+idtmp+'" >';
    tmphtml+=iedit+'</a>';
    tmphtml+='<a href="#" class="btn btn-xs btn-danger btneliminarpnl_speach" data-tipo="'+datos.tipo+'" data-pnlidgui="'+idtmp+'"><i class="fa fa-trash"></i></a></span></div>';
    //$('#preguntasDocente'+datos.pnlidgui).append(tmphtml);
}

$(document).ready(function(ev){
    if(!speachonline){
        var haypalabra=[];
        var alternativasspeach=[];
        var generargrama=function(){
            var ji=0;
            $('.plantilla-speach').each(function(i,v){
                ji++;
                var frtmp=$(v).find('.pnl-speach.docente .txtalu1').text().trim();
                frtmp=frtmp.replace(/[^a-zA-Z 0-9']+/gi,'');
                frtmp=frtmp.replace(/\s+/gi,' ');
                if(frtmp!=''||frtmp!=undefined){
                    farsesoffline.push({id:ji,frase:frtmp});
                    var txt=frtmp.toString().toUpperCase();
                    palabrasall=palabrasall+' '+txt;
                }
            })
            $.ajax({
                async:false,
                url: _sysUrlBase_+'/pronunciacion/buscarjson',
                data:{palabra:palabrasall}
            }).done(function(rs) {
                try{
                    var rs=JSON.parse(rs);
                    if(rs.code==200){
                        var dt=rs.data;                   
                        $.each(dt,function(i,v){                        
                            var I=i.toString().toUpperCase();
                            //console.log(v);
                            // var vpron = ''+v.pron
                            var V=v.pron.toString().toUpperCase();
                            var pt=[I,V];
                            if(haypalabra.indexOf(I)==-1){
                                wordList.push(pt);
                                haypalabra.push(I);
                            }
                            var alt=v.alt||'';
                            var alt2=[];
                            if(alt)
                                $.each(alt,function(j,k){
                                    var K=k.palabra.toString().toUpperCase();
                                    var Pr=k.pron.toString().toUpperCase();
                                    var pt2=[K,Pr];
                                    if(haypalabra.indexOf(K)==-1){
                                        wordList.push(pt2);
                                        haypalabra.push(K);
                                    }
                                    alt2.push({pa:K,pr:Pr});
                                });
                                alternativasspeach[I]=alt2;
                        })
                    }
                }catch(err){
                    console.log("Input is " + err);
                }
                
            });
            $.each(farsesoffline,function(i,t){
            var tsplit=t.frase.split(" ");
            var gramartmp = {numStates:tsplit.length, start: 0, end:tsplit.length-1, transitions:[]};
            for(j=0;j<tsplit.length;j++){
                var to=j==tsplit.length-1?j:j+1;
                var pal=tsplit[j].toString().toUpperCase();
                var tmptr={from: j, to: to, word:pal};
                gramartmp.transitions.push(tmptr);
                // console.log(alternativasspeach,pal);
                if(alternativasspeach[pal].length>0){
                    $.each(alternativasspeach[pal],function(ii,vv){
                        var tmptr={from: j, to: to, word:vv.pa};
                        gramartmp.transitions.push(tmptr);
                    });
                }
            }
            grammars.push({title: "gram"+i, g: gramartmp});
            })
        }
        generargrama();
    }
});
var speachpronunciar=function(pnl){ 
    var txt=$(pnl).find('.edittexto_ch').text();
    pronunciar(txt);    
}

var speach_inicializar=function(){}

var speach_iniciargrabacion=function(pnl,idgui){
    if(speachonline) speach_onlinegrabar(pnl,idgui);
    else speach_offlinegrabar(pnl,idgui);
}
var speach_detenergrabacion=function(pnl,idgui){
    if(speachonline) speach_onlinestop(pnl,idgui);
    else speach_offlinestop(pnl,idgui);
}

var streamtest=null;
var speach_onlinegrabar=function(pnl,idgui){
    chunks=[];
    if(!isMobile.any()){
        var jsonnavegador=isMobile.getBrowserInfo();           
        if(jsonnavegador.navegador=='Safari')
         /*navigator.mediaDevices.getUserMedia(constraints)
          .then(function(stream) {
            streamtest=stream;
            mediaRecorder = new MediaRecorder(stream);
            mediaRecorder.onstop = function(e){            
                blob = new Blob(chunks, { 'type' : 'audio/wav' });            
                speachuploadblob(blob,pnl); 
            }
            mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
            mediaRecorder.start();*/
            speachonlinefun(pnl,idgui);
          /*})
          .catch(function(error) {
           console.log("Error inesperado",error)
          });*/
        else
         navigator.getUserMedia({audio:true,video:false},function(stream){
            streamtest=stream;
            mediaRecorder = new MediaRecorder(stream);
            mediaRecorder.onstop = function(e){            
                blob = new Blob(chunks, { 'type' : 'audio/wav' });            
                speachuploadblob(blob,pnl); 
            }
            mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
            mediaRecorder.start();
            speachonlinefun(pnl,idgui);     
        },function(error){  
            alert(error);          
            console.log("Error inesperado",error)
        });
    }else{

      //  console.log('es mobil',isMobile.getBrowserInfo());
        speachonlinefun(pnl,idgui);
    }
}


var speachonlinefun=function(pnl,idgui){    
    //alert('entro aqui 3');
    //alert(window.webkitSpeechRecognition);
    
    if(speechRecognition == undefined){ console.log(window.speechRecognition); return false;}
    recognizeronline = new speechRecognition();
    //alert(recognizeronline);
    //alert('33');
    recognizeronline.continuous = true;
    recognizeronline.lang = "en-US";
    recognizeronline.interimResults = true;
    recognizeronline.onstart = function(){ 
        var btnGrabarAudio= pnl.find('.btnGrabarAudio');        
        let timen=3;
        var timerInterval = setInterval(() => {
            if(timen==0){
                clearInterval(timerInterval);
                btnGrabarAudio.removeClass('btn-danger disabled').addClass('btn-success').children('i').removeClass('fa fa-microphone-slash ').addClass('fa fa-microphone animated infinite zoomIn');
                btnGrabarAudio.find('span').text(btnGrabarAudio.find('span').attr('title'));
            }else 
            btnGrabarAudio.find('span').text(timen);
            timen--;
        },400);            
    }
    recognizeronline.onend = function(){ /*console.log('recong stop');*/
        //recognizeronline = null;
        //setTimeout(mostrartextospeach(tpl),7000);        
        var plantilla=pnl.closest('.plantilla-speach');
        plantilla.find('.btnPlaytexto').show();
        if(pnl.hasClass('alumno')&&isMobile.any()){//console.log('test');            
            plantilla.find('.btnGrabarAudio').trigger('click');            
            pnl.find('.btnPlaytexto').hide();
            setTimeout(textosimilar(plantilla),4000);
        }
    }   
    recognizeronline.onresult = function(event){
        var text = '';
        if(event.results &&  event.results.length )
        {
          for ( var i = event.resultIndex, len = event.results.length; i < len; ++i ){
            text += event.results[i][0].transcript;
          }
          pnl.find('.edittexto_ch.alumno').addClass('hide');
          pnl.find('.edittexto_ch').text(text);
        }
            
    }
    recognizeronline.start();
}
var speach_onlinestop=function(pnl,idgui){
    try{
    recognizeronline.stop();
    if(streamtest) streamtest.getTracks()[0].stop();
    mediaRecorder.stop();
    }catch(ex){}
}
var speach_offlinegrabar=function(pnl,idgui){
    chunks=[];
    speach_offlinestop();
    //navigator.mediaDevices.getUserMedia
    navigator.getUserMedia({audio: true},function startUserMedia(stream){
        streamtest=stream;
        var input = audioContext.createMediaStreamSource(stream);
        mediaRecorder = new MediaRecorder(stream);
        window.firefox_audio_hack = input; // Firefox hack https://support.mozilla.org/en-US/questions/984179
        var audioRecorderConfig = {errorCallback: function(x) {updateStatus("Error from recorder: " + x);}};
        recorder = new AudioRecorder(input, audioRecorderConfig);// If a recognizer is ready, we pass it to the recorder
        if (recognizer) recorder.consumers = [recognizer];
        isRecorderReady = true;
        updateUI();
        //var txt=$('#gramarid').val().trim();
        pnloffline=pnl.closest('.plantilla-speach');
        var txt=pnloffline.find('.pnl-speach.docente .txtalu1').text().trim();
        var id=0;
        //console.log(farsesoffline);
        $.each(farsesoffline,function(i,t){
            txt=txt.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
            var tf=t.frase.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
          if(txt.toString().toUpperCase()==tf.toString().toUpperCase()){
            id=t.id;
          }
        });
        mediaRecorder.onstop = function(e){            
            blob = new Blob(chunks, { 'type' : 'audio/wav' });            
            speachuploadblob(blob,pnl); 
        }
        mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
        //console.log(wordList,palabrasall,farsesoffline,grammars,id);
        if (recorder && recorder.start(id)) displayRecording(true);
        mediaRecorder.start();
        }, function(e) { updateStatus("No live audio input in this browser");
    });
}
var speach_offlinestop=function(pnl,idgui){
    try{
    if(streamtest) streamtest.getTracks()[0].stop();
    recorder && recorder.stop();
    if(mediaRecorder!=undefined)
    if(mediaRecorder.state!='inactive')  mediaRecorder.stop();
    pnloffline='';
    }catch(ex){console.log(ex)}
}

var speachuploadblob=function(miblob,pnl){
    var name=pnl.attr('id')||'';
    var data = new FormData();
    data.append('filearchivo',miblob);
    data.append('type',miblob.type);
    data.append('name',name);
    $.ajax({
        url : _sysUrlBase_+"/biblioteca/subirblob",
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        dataType :'json',
        success: function(res){
         if(res["code"]=='ok'){                      
             crearonda(res.namelink,pnl);
             var plantilla=pnl.closest('.plantilla-speach');
             if(pnl.hasClass('alumno'))setTimeout(textosimilar(plantilla),4000);
         }
        },error: function(e) {
            console.log('Error inesperado intententelo mas tarde',e);
        }
    });
}

var isurfer=0;
var wave=[];
var onda=function(id){
    if(wave[id]==undefined){
        wave[id] = Object.create(WaveSurfer); 
    }
    return wave[id];
}

function crearonda(url,donde){  
    if(url==''||url==undefined) return;
    var index1_=url.indexOf('static/media',0);
    var tmpurl=url.substring(index1_,10000);
    var iddonde=Date.now();
    var index2_=tmpurl.indexOf('.wav',0);   
    var urlingles=$('#PV_SE_URL').val()||_sysUrlBase_+'/';
    if(donde.hasClass('alumno')){
        urlingles=_sysUrlBase_+'/';
    }
    if(index2_!=-1){
        url=urlingles+tmpurl.substring(0,index2_+4)+'?tmp=aa'+iddonde;
    }else{
        index2_=tmpurl.indexOf('.mp3',0);
        url=urlingles+tmpurl.substring(0,index2_+4)+'?tmp=ee'+iddonde;
    }
    var idpnl=donde.attr('id');
    isurfer++;    
    var wavesurfer=null;
    if($('#'+idpnl).find('.wavesurfer').length>0){
        iddonde=$('#'+idpnl).find('.wavesurfer').attr('idtmp');        
        wavesurfer=onda(iddonde);
        wavesurfer.destroy();
        $('#'+idpnl).find('.wavesurfer').remove();
        $('#'+idpnl).find('audio').remove();        
    }
    $('#'+idpnl).find('.grafico').append('<div class="wavesurfer" idtmp="'+iddonde+'" id="wave_'+iddonde+'" ></div>');
    wavesurfer=onda(iddonde);
   // console.log('url-audio:'+url);
    wavesurfer.init({
        container: document.querySelector('#wave_'+iddonde),
        waveColor: '#A8DBA8',
        progressColor: '#3B8686',
        backend: 'MediaElement',
    });
    wavesurfer.load(url);
    wavesurfer.on('ready', function(){
      wavesurfer.drawer.container.style.display = '';
      wavesurfer.drawBuffer();
    });
    $('#'+idpnl+' .btnPlaytexto').off('click');
    $('#'+idpnl+' .btnPlaytexto').on('click',function(ev){
        $(this).addClass('inwave').attr('id','play_'+idpnl);
        wavesurfer.playPause();
    });
}

var mostrarondaSpeach=function($tpldocente){   
    if($tpldocente.length){   
        if($tpldocente.find('audio').length){            
            var url=$tpldocente.find('audio').attr('src');
            var donde=$tpldocente.find('.grafico');
            donde.html('');              
            crearonda(url,$tpldocente,true);
        }
    }
 }

var restextosimilar=function(plantilla,pt,res,np,nok){
    np=np||1;
    nok=nok||0; 
    id=Date.now();

    //$('#'+idgui).find('.textohablado').html(res+' '+'<span class="speachtexttotal" total-palabras="'+np+'" total-ok="'+nok+'">'+pt.toFixed(0)+'%</span>'+btn);
    //$('#btn'+id).trigger('click');
    var btn='<button class="btncalcular" id="btn'+id+'" data-result="'+pt+'" style="display:none;"></button>';
    plantilla.find('.textohablado').html(res+' '+'<span class="speachtexttotal" total-palabras="'+np+'" total-ok="'+nok+'">'+pt.toFixed(0)+'%</span>'+btn);
    plantilla.find('.textohablado').removeClass('hide');
    $('#btn'+id).trigger('click');
    //pausarTiempoSpeach();
}

var textosimilar=function(plantilla){
    var str1=plantilla.find('.pnl-speach.docente').find('.txtalu1').text()||'';
    var str2=plantilla.find('.pnl-speach.alumno').find('.txtalu2').text()||'';
    plantilla.find('.pnl-speach.alumno').find('.txtalu2').removeClass('hide');
    var stroriginal=str1;
    var pt=0;
    var result='';
    str1=str1.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
    str2=str2.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
    str3=str1.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
    str4=str2.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
    str5=str3.replace(/\s/gi,'');
    str6=str4.replace(/\s/gi,'');
    if(str5===str6){
        restextosimilar(plantilla,100,'<span class="speachtextook">'+stroriginal+'</span>',1,1);
        return;
    }
    if(str1.length==0) return;
    if(str2.length==0) return;
    if(str3===str4){
        restextosimilar(plantilla,100,'<span class="speachtextook">'+stroriginal+'</span>',1,1);
        return;
    }
    var _rstr1=str1.split(" ");
    var rstr1=str3.split(" ");
    var rstr2=str4.split(" ");  
    n1=rstr1.length;
    n2=rstr2.length;
    n3=n1>n2?n1:n2;
    var rstmp=[];
    var iok=0;
    for(var i=0; i<rstr1.length; i++){
        if(rstr1[i]==rstr2[i]){
            rstmp[i]='<span class="speachtextook">'+_rstr1[i]+'</span>';
            iok++
        }else{//ierror++;
            rstmp[i]='<span class="speachtextobad">'+_rstr1[i]+'</span>';
        }
    }
    var ok=iok;
    iok=((iok*100)/n3);
    restextosimilar(plantilla,iok,rstmp.join(' ').trim(),n3,ok);
}


var previewspeach=function(pnl,inpreview){
     var txtinfo=$(pnl).find('.textohablado >.speachtexttotal');
    if(txtinfo.length) return;
    $(pnl).find('.textohablado').html('');
    if(inpreview==true){
        $(pnl).find(' .inpreview').removeClass('hidden');        
        $(pnl).find('.pnl-speach.editable').removeClass('editable').attr('editable',"true");
        $(pnl).find(' .pnl-speach.inpreview .grafico').html('');
        $(pnl).find(' .pnl-speach .txtalu2').text('');
        $(pnl).find(' .pnl-speach.inpreview .btnPlaytexto').off('click');
        $(pnl).find(' .pnl-speach.alumno .btnGrabarAudio').removeClass('disabled').removeAttr('disabled');
    }else{

        $(pnl).find(' .inpreview').addClass('hidden');
        $(pnl).find('.pnl-speach[editable="true"]').addClass('editable').removeAttr('editable');
        $(pnl).find(' .pnl-speach.inpreview .grafico').html('');    
    }
}

var rinittemplatealshow=function(){
    var tabactivo=$('.tabhijo.active').find('.plantilla-speach');
    tabactivo.each(function(index, el){
        if($(el).hasClass('plantilla-speach')){
            var eldoncente=$(el).find('.docente');
            eldoncente.find('.btnPlaytexto').show();
            var isnone = eldoncente.find('.btnGrabarAudio').css("display");
            if(isnone!=undefined && isnone!='none'){
                if(!eldoncente.find('.btnadd_audio').length)
                eldoncente.find('.btnGrabarAudio').after(' <a class="btn btn-danger btnadd_audio nopreview"> <i class="fa fa-file-audio-o"></i> Add audio</a>');
            }
            var audiostmp= $(el).find('audio');
            audiostmp.each(function(i, audiotag){
                var url=$(audiotag).attr('src');
                var donde=$(audiotag).closest('.pnl-speach');
                donde.find('.grafico').html('');
                crearonda(url,donde,true);
            });
        }
    });
}

var initspeach=function(pnl){
    if(pnl=='')return;
    if(pnl.hasClass('editando')){
        previewspeach(pnl,false);
        $('#btns-guardar-actividad').show();
    }else{
        $('#btns-guardar-actividad').hide();
        previewspeach(pnl,true);       
    };
}

    var pausarTiempoSpeach = function(){
/*  var $tmplActiva = getTmplActiva();
    var $plantillaActiva = $tmplActiva.find('.plantilla-speach');
    $pnlalumno=$plantillaActiva.find('.pnl-speach.alumno');
    if($pnlalumno.find('.btnAccionspeachdby').length>0){
      $pnlalumno.find('.btnGrabarAudio').attr('disabled',true).addClass('disabled');
      var elemsTotal = $plantillaActiva.find('.textohablado span').length-1;
      var elemsCorregidas = $plantillaActiva.find('.textohablado span.speachtextook').length;
      //console.log(elemsTotal,elemsCorregidas);
      $('#panel-tiempo .info-show').trigger('oncropausar');
      $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
      var $panelIntentos = $('#panel-intentos-dby');
      var intento_actual = $panelIntentos.find('.actual').text();
      var intentos_total = $panelIntentos.find('.total').text();
      if(( elemsTotal === elemsCorregidas && elemsTotal>0)||intento_actual>=intentos_total){
        $tmplActiva.find('.save-progreso').show('fast');
      }
    }*/
}