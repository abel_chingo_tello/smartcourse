/*
Autor: abel chingo tello
require jquery y moment
*/

$.fn.achtcronometro= function(opt){
return this.each(function(){		 
    var el = $(this);
    var op=opt||{};
    var sy=op.start||false;    
    var it=op.timeinicio||el.val()||el.text()||'00:00:00';
    var dn=op.donde||el.attr('data-donde')||'';
    var ft=op.terminaren||el.attr('data-fin')||'00:00:00';
    var tp=op.tipo||el.attr('data-tipo')||'s';  //s =suma , r=resta;
    var st=el.attr('data-iniciar')||'s';
    var rf=op.formato||el.attr('data-returnformat')||'HH:mm:ss';
    var cp=op.callbackpausar;
    var cr=op.callbackreset;
    var cs=op.callbackstart;
    var ct=op.callbacktermino;
    el.attr('data-start',it);
    it=moment(it,'HH:mm:ss').format('HH:mm:ss');
    ft=moment(ft,'HH:mm:ss').format('HH:mm:ss');
    var inicio=false;
    var tc;
    if(dn==''){if(el.is('input')){dn='value';}else{dn='text';}}
	var iniciar=function(){
		var hora=moment(it,'HH:mm:ss');
		tc=setInterval(function(){
			if($(el).length==0) {clearInterval(tc); inicio=false;}
			if(tp=='r')hora.subtract(1, 'seconds');
			else hora.add(1, 'seconds');
			var newtime=moment(hora,'HH:mm:ss').format('HH:mm:ss');
			var rt=moment(hora,'HH:mm:ss').format(rf);
			if(dn=='value') el.val(rt);
			else if(dn=='text') el.text(rt);
			else if(dn=='') el.attr('data-time',rt);
			else el.attr(dn,rt);
			if(tp=='r'&&ft=='00:00:00'&&newtime==ft) el.trigger('termino');
			else if(it!=ft&&newtime==ft) el.trigger('termino'); 
		},1000);
	}	
	el.on('iniciar',function(ev){if(inicio==false){ inicio=true;iniciar();if(typeof cs !== 'undefined' && jQuery.isFunction(cs))cs();}});
	el.on('pausar',function(ev){if(inicio==true){clearInterval(tc);if(typeof cp !== 'undefined' && jQuery.isFunction(cp))cp();}});
	el.on('reset',function(ev){	if(inicio)clearInterval(tc);it=el.attr('data-start');if(typeof cr !== 'undefined' && jQuery.isFunction(cr))cr();});
	el.on('termino',function(ev){if(inicio){clearInterval(tc);inicio=false;	if(typeof ct !== 'undefined' && jQuery.isFunction(ct))ct();}});
	if(st=='s'||st=='iniciar'||sy) el.trigger('iniciar');
});
};