$(document).ready(function(){
   	var changeButtonIcon = function(idBtn, oldIcon, newIcon){
        $(idBtn+" i").removeClass(oldIcon);
        $(idBtn+" i").addClass(newIcon);
    };

 
    var loadPHPSource = function( source , container ){
        var path = "";
        var ext = '.php';
        if(source != null){
            container.load( path+source+ext );
        }
        else{
            container.load( path+"_blank"+ext );
        }
    };
    $('body').on('click','.iralink',function(ev){
        ev.preventDefault();
        var href=$(this).attr('data-href')||'';
        __sysAyax({ showmsjok:true,  url:_sysUrlBase_+href, callback:function(rs){
        if(rs.code==200) 
            window.location.href=_sysUrlSitio_; 
        } });
    })
     
});


/**
* Funcion general para llamar a Biblioeca
* Incializar asi: $(selectorBtn).biblioteca();
* @required: "selectedfile()" at "./static/tema/js/funciones.js"
**/
/*(function($){
    $.fn.biblioteca=function(){
        var that = $(this);
        that.on('click', function(ev) {
            ev.preventDefault();
            var txt='Select or Upload';
            selectedfile(ev,this,txt);
        });
    };
}(jQuery));*/