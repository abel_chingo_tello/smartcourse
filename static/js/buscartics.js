var tabledatosabc3='';
var _sysUrlRaiz_='<?php echo URL_RAIZ; ?>';
console.log(_sysUrlRaiz_);
function refreshdatosabc3(){
    tabledatosabc3();
}
$(document).ready(function(){  
  var estadosabc3={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}; 
 $('#ventana-abc3')
  .on('click','.btnbuscar',function(ev){
    refreshdatosabc3();
  }).on('keyup','#textoabc3',function(ev){
    if(ev.keyCode == 13)
    refreshdatosabc3();
  }).on("mouseenter",'.panel-user', function(){
      if(!$(this).hasClass('active')){ 
        $(this).siblings('.panel-user').removeClass('active');
        $(this).addClass('active');
      }
  }).on("mouseleave",'.panel-user', function(){     
      $(this).removeClass('active');
  }).on("click",'.btn-selected', function(){
      $(this).addClass('active');
      let pnl=$(this).closest('.panel-user');
      let idtic=pnl.attr('data-id');
      let idsesion=pnl.attr('data-idsesion');
      /*<?php if(!empty($ventanapadre)){?>
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idtic',idtic);
            btn.attr('data-idsesion',idsesion);
            btn.trigger("addtics");
          }
      <?php }  ?>*/
     $(this).closest('.modal').find('.cerrarmodal').trigger('click');   
  });

  tabledatosabc3=function(){
    var frm=document.createElement('form');
    var formData = new FormData();
        formData.append('texto', $('#textoabc3').val()||''); 
        formData.append('met','<?php echo $met; ?>');       
        var url= _sysUrlRaiz_+'Actividad/buscarActividadjson/';
        $.ajax({
          url: url,
          type: "POST",
          data:  formData,
          contentType: false,
          processData: false,
          dataType :'json',
          cache: false,
          beforeSend: function(XMLHttpRequest){
            $('#datalistadoabc3 #cargando').show('fast').siblings('div').hide('fast');
          },      
          success: function(res)
          {                      
           if(res.code=='ok'){
              var midata=res.data;
              var html='';
              if(midata!=undefined){               
                var controles=$('#controlesabc3').html();
                $.each(midata,function(i,v){

                  var urlimg=_sysUrlRaiz_+'media/imagenes/cursos/nofoto.jpg';
                  var urlimgtmp=_sysUrlRaiz_+(v.imagen.replace(/__xRUTABASEx__\//gi,''));
                  var srcimg=_sysfileExists(urlimgtmp)?(urlimgtmp):(urlimg);
                  html+='<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="'+v.idactividad+'" data-idsesion="'+v.sesion+'">'+controles;
                  html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%"></div>'; 
                  html+='<div class="nombre"><strong>'+v.nombre+'</strong></div>';
                  html+='</div></div>';                  
                });
                $('#datalistadoabc3 #data').html(html).show('fast').siblings('div').hide('fast');
              }else     
                $('#datalistadoabc3 #sindatos').show('fast').siblings('div').hide('fast');
            }else{
              $('#datalistadoabc3 #error').html(res.message).show('fast').siblings('div').hide('fast');
            }
          },
          error: function(e) 
          {
            $('#datalistadoabc3 #error').show('fast').siblings('div').hide('fast');
          }
        });
  }
  tabledatosabc3();
});