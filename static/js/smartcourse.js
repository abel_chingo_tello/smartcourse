var pltcnt1 = 'editar';
var cntpn1 = 'cntm1';
var optionslike = {
    //dots: true,
    infinite: false,
    //speed: 300,
    //adaptiveHeight: true
    navigation: false,
    slidesToScroll: 1,
    centerPadding: '60px',
    slidesToShow: 5,
    responsive: [
        { breakpoint: 1200, settings: { slidesToShow: 5 } },
        { breakpoint: 992, settings: { slidesToShow: 4 } },
        { breakpoint: 880, settings: { slidesToShow: 3 } },
        { breakpoint: 720, settings: { slidesToShow: 2 } },
        { breakpoint: 320, settings: { slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/ } }
    ]
};
var __idgui = function() {
    return Date.now() + (Math.floor(Math.random() * (1 - 100)) + 1);
}
var __isdom = function(el) {
    try {
        if (el.length) return true;
        else return false;
    } catch (er) { return false }
}

var __isEmpty = function(el) {
    return !$.trim(el.html());
}

var __copyhtml = function(html) {
    var copyhtml = document.querySelector('#copyallhtml');
    copyhtml.value = html;
    copyhtml.select();
    rs = document.execCommand("copy");
    copyhtml.value = '';
}

var __initEditor = function(tmpid, mplugins) {
    console.log('hola');
    var mplugins = mplugins || '';
    tinymce.init({
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,
        convert_newlines_to_brs: true,
        menubar: false,
        //statusbar: false,
        verify_html: false,
        content_css: [_sysUrlBase_ + '/static/tema/css/bootstrap.min.css'],
        selector: '#' + tmpid,
        height: 210,
        paste_auto_cleanup_on_paste: true,
        paste_preprocess: function(pl, o) {
            var html = '<div>' + o.content + '</div>';
            var txt = $(html).text();
            o.content = txt;
        },
        paste_postprocess: function(pl, o) {
            o.node.innerHTML = o.node.innerHTML;
            o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
        },
        plugins: [mplugins + " textcolor paste lists advlist"], // chingoinput chingoimage chingoaudio chingovideo
        toolbar: ' | undo redo ' + mplugins + ' | styleselect | removeformat |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect| ', //chingoinput chingoimage chingoaudio chingovideo,
        advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
    });
}

var __mostrarPage = function(id) {
    var pages = $('#mainpage').find('.seepage');
    var pageid = $('#mainpage').find('.seepage.mostrarpagina').attr('id') || '';
    var firstPage = pages.first().attr('id') || '-1';
    var lastPage = pages.last().attr('id') || '-1';
    if (pageid == id) return;
    if (firstPage == id) {
        $('.btnnavegacionPage').children('.btnirback').attr('disabled', true).removeClass('btn-primary').addClass('btn-default');
        $('.btnnavegacionPage').children('.btnirmenu').attr('disabled', true).removeClass('btn-primary').addClass('btn-default');
    } else {
        $('.btnnavegacionPage').children('.btnirback').attr('disabled', false).removeClass('btn-default').addClass('btn-primary');
        $('.btnnavegacionPage').children('.btnirmenu').attr('disabled', false).removeClass('btn-default').addClass('btn-primary');
    }
    if (lastPage == id) $('.btnnavegacionPage').children('.btnirnext').attr('disabled', true).removeClass('btn-primary').addClass('btn-default');
    else $('.btnnavegacionPage').children('.btnirnext').attr('disabled', false).removeClass('btn-default').addClass('btn-primary');
    $_cont = $('#' + id).find('._cont');
    $.each($_cont, function(i, v) {
        var cargaren = $(v).attr('data-cargaren') || '';
        var cargado = $(v).attr('data-cargado') || 'no';
        if (cargaren != '' && cargado == 'no') {
            $iframe = $('#' + cargaren);
            if ($iframe.length) {
                $iframe.attr('src', $iframe.attr('data-olmedia'));
                $(v).attr('data-cargado', 'si');
            }
        }
    });
    $('#' + id).siblings('.mostrarpagina').fadeOut().removeClass('mostrarpagina');
    $('#' + id).fadeIn().addClass('mostrarpagina').animateCss({ 'de': 'in' });
}

var __cargarplantilla = function(obj) {
    var obj = obj || {};
    var url = donde = vista = false;
    var fncall = obj.fncall || '';
    url = obj.url || false;
    donde = obj.donde || cntpn1;
    vista = obj.vista || false;
    data = obj.data || {};
    data.idioma = _sysIdioma_;
    data.vista = vista;
    data.plt = 'blanco';
    if (!url) return false;
    __sysajax({
        url: _sysUrlBase_ + url,
        fromdata: data,
        type: 'html',
        donde: $('#' + donde),
        mostrarcargando: false,
        callback: function(rs) {
            $('#' + donde).html(rs);
            if (_isFunction(fncall)) fncall(rs);
        }
    });
}

var __cargarWidget = function(obj, donde) {
    var obj = obj || {};
    url = obj.url || false;
    data = obj.data || {};
    data.idioma = _sysIdioma_;
    data.vista = vista;
    data.plt = 'blanco';
    if (!url) return false;
    if (donde.length == 0) return;
    __sysajax({
        url: _sysUrlBase_ + url,
        fromdata: data,
        type: 'html',
        donde: donde,
        mostrarcargando: false,
        callback: function(rs) {
            donde.html(rs);
        }
    });
}

var __subirmediaaweb = function(obj, attr, fn) {
    var _tmpfyle = attr.file; // imagen
    var _inputfile = attr.inputfile; // input.file
    var file = '';
    if (_inputfile.length == 0) {
        file = document.createElement('input');
        file.id = 'file_' + __idgui();
        file.type = 'file';
    } else {
        var tmpid = 'file_' + __idgui();
        _inputfile.attr('id', tmpid);
        _inputfile.off('change');
        file = document.getElementById(tmpid);
    }
    file.accept = attr.accept;
    file.addEventListener('change', function(ev) {
        var reader = new FileReader();
        reader.onload = function(filetmp) {
            var filelocal = filetmp.target.result;
            _tmpfyle.attr('src', filelocal);
        }
        reader.readAsDataURL(this.files[0]);
        if (attr.guardar) {
            var $idprogress = $('#clone_progressup').clone(true);
            var iduploadtmp = 'idup' + __idgui();
            $idprogress.attr('id', iduploadtmp);
            obj.append($idprogress);
            var data = __formdata('');
            data.append('media', this.files[0]);
            data.append('dirmedia', attr.dirmedia);
            data.append('oldmedia', attr.oldmedia);
            data.append('typefile', attr.typefile);
            __sysajax({
                fromdata: data,
                url: _sysUrlBase_ + '/acad_cursodetalle/uploadmedia',
                iduploadtmp: '#' + iduploadtmp,
                callback: function(rs) {
                    if (rs.code == 'ok') {
                        if (rs.media != '') {
                            _tmpfyle.attr('src', rs.media);
                            _tmpfyle.attr('data-olmedia', rs.media);
                            fn(rs);
                        }
                    }
                }
            });
        }
    });
    file.click();
}

var __subirfile = function(obj, fn) {
    var opt = { file: '', typefile: '', guardar: false, dirmedia: '', oldmedia: '', uploadtmp: false, fntmp: 'h' };
    $.extend(opt, obj);
    var mostraren = __isdom(opt.file) ? opt.file : false;
    var acepta = '';
    if (opt.typefile == 'imagen') { acepta = 'image/x-png, image/gif, image/jpeg, image/*';
        opt.uploadtmp = true; } else if (opt.typefile == 'audio') acepta = 'audio/mp3';
    else if (opt.typefile == 'video') acepta = 'video/mp4, video/*';
    else if (opt.typefile == 'html') acepta = '.html , .htm , application/zip';
    else if (opt.typefile == 'flash') acepta = '.swf , .flv  , application/zip';
    else if (opt.typefile == 'pdf') acepta = 'application/pdf , application/zip';
    var file = document.createElement('input');
    file.id = 'file_' + __idgui();
    file.type = 'file';
    file.accept = acepta;
    file.addEventListener('change', function(ev) {
        if (opt.uploadtmp && mostraren) {
            var rd = new FileReader();
            rd.onload = function(filetmp) {
                var filelocal = filetmp.target.result;
                mostraren.attr('src', filelocal);
                var fntm = opt.fntmp;
                if (_isFunction(fntm)) fntm(file);
            }
            rd.readAsDataURL(this.files[0]);
        }
        if (opt.guardar) {
            var iduploadtmp = '';
            if (mostraren) {
                var $idprogress = $('#clone_progressup').clone(true);
                iduploadtmp = 'idup' + __idgui();
                $idprogress.attr('id', iduploadtmp);
                mostraren.closest('div').append($idprogress);
                opt.oldmedia = mostraren.attr('oldmedia') || '';
                iduploadtmp = '#' + iduploadtmp;
            }
            var data = new FormData()
            data.append('media', this.files[0]);
            data.append('dirmedia', opt.dirmedia);
            data.append('oldmedia', opt.oldmedia);
            data.append('typefile', opt.typefile);
            __sysajax({
                fromdata: data,
                url: _sysUrlBase_ + '/acad_cursodetalle/uploadmedia',
                iduploadtmp: iduploadtmp,
                callback: function(rs) {
                    if (rs.code == 'ok') {
                        if (rs.media != '') {
                            if (mostraren) {
                                mostraren.attr('src', rs.media);
                                mostraren.attr('data-olmedia', rs.media);
                            }
                            if (_isFunction(fn)) fn(rs);
                        }
                    }
                }
            });
        }
    });
    file.click();
}


var __subirmediamdini = function(obj, type, typefile) {
    var oldmedia = obj.attr('data-oldmedia') || '';
    var file = document.createElement('input');
    file.type = 'file';
    file.accept = type;
    file.id = 'file_' + __idgui();
    file.addEventListener('change', function(ev) {
        var $idprogress = $('#clone_progressup').clone(true);
        var iduploadtmp = 'idup' + __idgui();
        $idprogress.attr('id', iduploadtmp);
        obj.append($idprogress);
        var data = __formdata('');
        data.append('media', this.files[0]);
        data.append('dirmedia', 'curso_' + idcurso + '/');
        data.append('oldmedia', oldmedia);
        data.append('typefile', typefile);
        __sysajax({
            fromdata: data,
            url: _sysUrlBase_ + '/acad_cursodetalle/uploadmedia',
            iduploadtmp: '#' + iduploadtmp,
            callback: function(rs) {
                if (rs.code == 'ok') {
                    if (rs.media != '') {
                        __cargarplantilla({ url: '/plantillas/' + typefile, donde: 'contentpages', data: { link: rs.media } });
                        //obj.attr('data-oldmedia',rs.media).children('i').addClass('btn-success').removeClass('btn-primary'); 
                    }
                }
            }
        });
    });
    file.click();
}

$.fn.menucircle = function(opt) {
    var opt = opt || {};
    var ruta = opt.ruta || '';
    //console.log(opt);
    return this.each(function() {
        var anihover = opt.ani1 || 'hvr-grow hvr-glow';
        var el = $(this);
        var openmenu = el.siblings('.openmenucircle');
        var menumedidas = function(obj) {
            var witget = obj.closest('.widgetcont');
            var widget_alt = parseInt(witget.height().toFixed(2));
            var witget_anc = parseInt(witget.width().toFixed(2));
            var nav = obj.closest('.nav');
            var heightrowtexto = parseInt(witget.find('.row').first().height().toFixed(2));
            var navrow = nav.closest('.row');
            navrow.css({ 'height': 'calc(100% - ' + (heightrowtexto + 3) + 'px)' });
            var anchonav = parseInt(navrow.width().toFixed(2));
            var altonav = parseInt(navrow.height().toFixed(2));
            var htmp = 100;
            var tamtemp = 0;
            if (witget_anc <= widget_alt) {
                tamtemp = anchonav + 'px';
                htmp = parseInt((anchonav / 7).toFixed(2));
            } else {
                tamtemp = altonav + 'px';
                htmp = parseInt((altonav / 7).toFixed(2));
            }
            nav.css({ 'width': tamtemp, 'height': tamtemp });
            openmenu.css({ 'left': 'calc(50% - ' + (htmp / 2) + 'px )', 'top': 'calc(50% - ' + (htmp / 2) + 'px )', 'width': htmp, 'height': htmp });
            return htmp;
        }

        var __frmiedit = function(obj) {
            var tmpitem = obj.parent().parent();
            var spantxt = tmpitem.find('span').first();
            var mdfuente = spantxt.attr('data-fuente') || '';
            var mdfuentezise = spantxt.attr('data-fuentezise') || '';
            var mdcolortexto = spantxt.attr('data-colortexto') || '';
            var mdcolorfondo = tmpitem.attr('data-colorfondo') || '';
            var imgfondo = tmpitem.attr('data-olmedia') || '';
            var mdlink = tmpitem.attr('data-linkid') || '';
            var mdtipolink = tmpitem.attr('data-linktipo') || 'IN';
            //var mdtipolink=tmpitem.attr('data-linkid')||'';
            var $md = __sysmodal({ titulo: '', html: $('#clone_editmenuwitget').html() });
            $md.find('input.texto').val(spantxt.text().trim());
            $md.find('select.tipofuente').val(mdfuente.trim());
            $md.find('select.sizefuente').val(mdfuentezise.trim());
            $pages = $('#mainpage').find('.seepage');
            $md.find('select.link').empty();
            $md.find('select.link').append('<option></option>');
            $.each($pages, function(i, v) {
                var idlink = $(v).attr('id')
                var sel = mdlink == idlink ? 'selected="selected"' : '';
                var opt = '<option value="' + idlink + '" ' + sel + ' >' + $(v).children('._acccont').children('span').children('span.name').text() + '</option>';
                $md.find('select.link').append(opt);
            })
            $md.find('select.link').append('<option value="-1">Otro link</option>');
            $md.find('select.link').val(mdlink.trim()).attr('data-linktipo', mdtipolink);
            if (mdtipolink == 'OUT') $md.find('.otrolink').fadeIn();

            if (mdcolortexto != '') {
                $md.find('input.colortexto').val(mdcolortexto).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
            }
            if (mdcolorfondo != '') {
                $md.find('input.colorfondo').val(mdcolorfondo).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
            }
            $md.find('input.texto').on('change', function() { spantxt.text($(this).val()); });
            $md.find('select.tipofuente').on('change', function() { spantxt.attr('data-fuente', $(this).val());
                spantxt.css({ 'font-family': $(this).val() }); });
            $md.find('select.sizefuente').on('change', function() { spantxt.attr('data-fuentezise', $(this).val());
                spantxt.css({ 'font-size': $(this).val() }); });
            if (imgfondo != '') $md.find('img').attr('src', imgfondo).attr('data-olmedia', imgfondo);
            $md.find('input.colortexto').on('change', function() {
                mdcolortexto = $(this).hasClass('sincolor') ? '' : $(this).val();
                spantxt.attr('data-colortexto', mdcolortexto);
                spantxt.css({ 'color': mdcolortexto });
            });
            $md.find('input.colorfondo').on('change', function() {
                mdcolorfondo = $(this).hasClass('sincolor') ? '' : $(this).val();
                tmpitem.attr('data-colorfondo', mdcolorfondo);
                tmpitem.css({ 'background': mdcolorfondo });
            });
            $md.find('select.link').on('change', function() {
                mdlink = $(this).val();
                mdtipolink = mdlink == '-1' ? 'OUT' : 'IN';
                //mdtipolink=$(this).attr('data-linktipo');
                tmpitem.attr('data-linkid', mdlink);
                tmpitem.attr('data-linktipo', mdtipolink);
                if (mdtipolink == 'OUT') $md.find('.otrolink').fadeIn();
                else $md.find('.otrolink').fadeOut();
            });
            $md.find('input.otrolink').on('change', function() {
                mdlink = $(this).val();
                tmpitem.attr('data-linkid', mdlink);
                tmpitem.attr('data-linktipo', 'OUT');
            });
            $md.on('click', '.subirimg', function(ev) {
                ev.preventDefault();
                ev.stopPropagation();
                var btn = $(this)
                var $file1 = btn.parent().siblings('input[type="file"]');
                var $tmpmedia = btn.parent().siblings('img');
                $file1.off('change');
                $file1.on('change', function() {
                    var reader = new FileReader();
                    reader.onload = function(filetmp) {
                        var filelocal = filetmp.target.result;
                        $tmpmedia.attr('src', filelocal);
                    }
                    reader.readAsDataURL(this.files[0]);
                    var $idprogress = $('#clone_progressup').clone(true);
                    var iduploadtmp = 'idup' + Date.now() + Math.floor((Math.random() * 100) + 1);
                    $idprogress.attr('id', iduploadtmp);
                    $(this).closest('.frmchangeimage').append($idprogress);
                    tmpolmedia = $tmpmedia.attr('data-olmedia') || '';
                    var data = __formdata('');
                    data.append('media', this.files[0]);
                    data.append('dirmedia', 'curso_' + opt.idcurso + '/');
                    data.append('oldmedia', tmpolmedia);
                    //data.append('typefile',type);
                    __sysajax({
                        fromdata: data,
                        url: _sysUrlBase_ + '/acad_cursodetalle/uploadmedia',
                        iduploadtmp: '#' + iduploadtmp,
                        callback: function(rs) {
                            if (rs.code == 'ok') {
                                if (rs.media != '') {
                                    $tmpmedia.attr('src', rs.media);
                                    tmpitem.css({ 'background-image': 'url(' + rs.media + ')' });
                                    tmpitem.attr('data-olmedia', rs.media);
                                }
                            }
                        }
                    });
                });
                $file1.trigger('click');
            });
        }
        el.on('graficar', function(ev) {
            h = menumedidas(el);
            var items = $(this).children();
            var l = items.length;
            $.each(items, function(i, v) {
                v.style.left = 'calc(' + ((50 - 30 * Math.cos(-0.5 * Math.PI - 2 * (1 / l) * i * Math.PI)).toFixed(4)) + "% - " + (h / 2) + 'px)';
                v.style.top = 'calc(' + ((50 + 30 * Math.sin(-0.5 * Math.PI - 2 * (1 / l) * i * Math.PI)).toFixed(4)) + "% - " + (h / 2) + 'px)';
                $(v).css({ 'width': h, 'height': h, 'overflow': 'hidden', 'cursor': 'pointer' }).removeClass(anihover).addClass(anihover);
            })
        })
        el.on('click', '.iremove', function(ev) {
            $(this).closest('.menucircleitem').remove();
            el.trigger('graficar');
        })
        el.on('click', '.iedit', function(ev) {
            ev.preventDefault();
            var item = $(this).closest('.menucircleitem');
            var txt = item.children('span:first');
            __paneltools({
                item: item,
                texto: txt.text(),
                fncall: function(rs) {
                    if (rs.texto = !'') txt.text(rs.texto);
                    else txt.text('');
                    if (rs.colorfondo == '') itema.attr('data-colorfondo', 'transparent').css({ 'background-color': 'transparent' });
                    else itema.attr('data-colorfondo', rs.colorfondo).css({ 'background-color': rs.colorfondo });
                    if (rs.colortexto != '') itema.attr('data-colorfondo', rs.colortexto).css({ 'color': rs.colortexto });
                    if (rs.imagenfondo.trim() != '') itema.css({ 'background-image': 'url(' + rs.imagenfondo + ')' });
                    else itema.css({ 'background-image': 'none' });
                    if (rs.datalink.trim() != '') itema.attr('data-link', rs.datalink);
                    if (rs.datatype.trim() != '') itema.attr('data-type', rs.datatype);
                    if (rs.fuente.trim() != '') itema.attr('data-fuente', rs.fuente).css({ 'font-family': rs.fuente });
                    if (rs.zise.trim() != '') itema.attr('data-zise', rs.zise).css({ 'font-size': rs.zise })
                    itema.attr('data-donde', 'contentpages');
                    itema.trigger('click');
                }
            });
            //__frmiedit($(this));

            ev.stopPropagation();
        })
        openmenu.on('click', '.iedit', function(ev) {
            ev.preventDefault();
            __frmiedit($(this));
            ev.stopPropagation();
        })
        openmenu.on('click', '._additem', function(ev) {
            var cl = '<div class="menucircleitem"><span> texto </span> <span class="options noalumno"><i class="fa fa-pencil iedit"></i> <i class="fa fa-trash iremove"></i></span></div>';
            var pt = $(this).closest('.openmenucircle').siblings('.menucircle');
            pt.append(cl);
            el.trigger('graficar');
        })
        openmenu.addClass('hvr-grow hvr-glow');
        el.trigger('graficar');
        $(window).resize(function() {
            el.trigger('graficar');
        })
        return el;
    });
}