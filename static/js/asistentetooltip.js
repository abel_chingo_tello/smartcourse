var estainteractuando = true;
var revisarInactividad = function () {
    var t;
    window.onload = reiniciarTiempo;
    document.onmousemove = reiniciarTiempo;
    document.onkeypress = reiniciarTiempo;
    document.onload = reiniciarTiempo;
    document.onmousemove = reiniciarTiempo;
    document.onmousedown = reiniciarTiempo;
    document.ontouchstart = reiniciarTiempo;
    document.onclick = reiniciarTiempo; 
    document.onscroll = reiniciarTiempo;
    document.onkeypress = reiniciarTiempo;
    function tiempoExcedido() {
        estainteractuando = false;
        revisarInactividad();
    }

    function reiniciarTiempo() {
        clearTimeout(t);
        t = setTimeout(tiempoExcedido, 3000)
        estainteractuando = true;
    }
};
revisarInactividad();

var _tooltipactual='.tooltip1';
var _asistentetooltipcontador=0;
var t1;
var asistentetooltip=function(){
	//console.log(_tooltipactual);
	//if(estainteractuando==false){
		if(t1!=null) clearTimeout(t1);
		_asistentetooltipcontador++;

		if(isMobile.any()!=null){
			let $tooltiptmp=$(_tooltipactual);
			//console.log(_asistentetooltipcontador,$tooltiptmp);
			if(_asistentetooltipcontador<10 && $tooltiptmp.length){
				$(_tooltipactual).tooltip('toggle');
				if($('.tooltipstatic > .tooltip').length)
				if($('.tooltipstatic > .tooltip').is(':visible')){
					$('.tooltipstatic > .tooltip').hide();
				}else $('.tooltipstatic > .tooltip').show();
				
			}
		}

		t1=setTimeout(function(){		
			if(_asistentetooltipcontador==20) _asistentetooltipcontador=0;
			asistentetooltip();
		},1500);
	//}
};

var inicio=false;
$(document).ready(function(){
	var _ismobiletmp=isMobile.any();
	console.log('_ismobiletmp',_ismobiletmp);	
	asistentetooltip();
	$('body').on('click','.asistentetooltip',function(ev){		
		let $tooltiptmp=$(_tooltipactual);
		_tooltipactual=$(this).attr('shownexttooltip')||'';
		setTimeout(function(){$tooltiptmp.tooltip('hide'); $('.tooltipstatic > .tooltip').hide();},1000);
	})
	$('body').click(function(){
		$('[data-toggle="tooltip"]').tooltip('hide');
		$('.tooltipstatic > .tooltip').hide();
	})
	$('[data-toggle="tooltip"]').tooltip();
    //$('[data-toggle="popover"]').popover();	
});