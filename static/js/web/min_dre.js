function min_dre(md){
	var id_ubigeo=md.find('#txtUbigeo').attr('datavalue');
	var idgui=md.find('form').attr('idgui');
	var _cargarcombos=function(){
    var fd = new FormData();
        fd.append("provincia", "00");
        fd.append("distrito", "00");
        fd.append("pais", "PE");       
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ubigeo',
      callback:function(rs){
        $sel=md.find('#txtUbigeo');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.id_ubigeo+'" '+(v.id_ubigeo==id_ubigeo?'selected="selected"':'')+ '>'+v.id_ubigeo +" : "+ v.ciudad+'</option>');
          })
      }})}
    _cargarcombos();
	md.on('click','.btn-activar',function(){
	    var i=$("i",this);  
	    if(i.hasClass('fa-circle')){
	      $(this).addClass('active');
	      $('input',this).val(1);
	      i.removeClass('fa-circle').addClass('fa-check-circle');
	    }else{
	      $(this).removeClass('active');
	      $('input',this).val(0);
	      i.addClass('fa-circle').removeClass('fa-check-circle');
	    }     
	}).on('click','.subirfile',function(ev){
	      __subirfile($(this));
	}).on('submit','#frm-'+idgui,function(ev){
      	ev.preventDefault();
      	var datareturn=$(this).attr('datareturn')||'';
        var fele = document.getElementById("frm-"+idgui);
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/min_dre/guardar',
              showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
					if(datareturn!='') md.trigger(datareturn);
					else redir(_sysUrlSitio_+'min_dre');
                }
              }
        });
    })
}

function min_dreimport(md){
  md.find('.modal-dialog').removeClass('modal-lg').addClass('modal-xl');
	var idgui=md.find('table.formventana').attr('idgui');
	md.on('click','.btnimport'+idgui,function(ev){
		var clone = document.getElementById('importjson').cloneNode();
  		clone.value = '';
      md.find('#contenfile').html(clone);
      md.find('#importjson').trigger('click');
    })

    md.on('click','.btnsaveimport'+idgui,function(ev){
    var datareturn=$('#datosimport'+idgui+' table').attr('datareturn')||'';
    var tableimport=md.find('table.tableimport'+idgui);
    if(tableimport.length){
      var tr=tableimport.find("tr.addregistroimporttr");
      var datosimportados=[];
      $.each(tr,function(i,v){
        var data={
          iddre:$(v).find("td:eq(0)").text().trim(),
          descripcion:$(v).find("td:eq(1)").text().trim(),
          ubigeo:$(v).find("td:eq(2)").text().trim(),
          opcional:$(v).find("td:eq(3)").text().trim(),
        }
        datosimportados.push(data);
      });
     
      var formData = new FormData();
      formData.append('datajson', JSON.stringify(datosimportados));
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'json/min_dre/importar',
        showmsjok : true,
        callback:function(data){
            if(datareturn!='') md.trigger(datareturn);
			else redir(_sysUrlBase_+'min_dre');
        }
      }
      __sysAyax(data);
      return false;
    }    
  }).on('click','.btn_removetr',function(ev){
      $(this).closest('tr').remove();
  }).on('change','#importjson',function(ev){
  	var table=md.find('#datosimport'+idgui);
  	var donde=md.find('#paneldatosaimportar'+idgui);
  	do_file(ev.target.files,{table:table,hoja:1,col:4,fil:4,donde:donde});
  	md.find('#pnlbotonesde'+idgui).hide();
    md.find('#paneldatosaimportar'+idgui).show();
  }).on('click','.btncancel',function(ev){
  	//var datareturn=$('#datosimport'+idgui+' table').attr('datareturn')||'';
  	//if(datareturn!='') md.trigger(datareturn);
	//else redir(_sysUrlSitio_+'/min_dre');
  	md.find('#pnlbotonesde'+idgui).show();
    md.find('#paneldatosaimportar'+idgui).hide();
  })
  md.find('.hide').hide();
}

