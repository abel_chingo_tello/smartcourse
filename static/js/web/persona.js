function frmpersona(md){
  //var id_ubigeo=md.find('#txtUbigeo').attr('datavalue');
  var _imgdefecto='static/media/usuarios/user_avatar.jpg';
  var idgui=md.find('form').attr('idgui'); 
  md.on('click','.btn-activar',function(){
      var i=$("i",this);  
      if(i.hasClass('fa-circle-o')){
        $(this).addClass('active');
        $('input',this).val(1);
        $('span',this).text($(this).attr('dataactive'));
        i.removeClass('fa-circle-o').addClass('fa-check-circle');
      }else{
        $(this).removeClass('active');
        $('input',this).val(0);
        $('span',this).text($(this).attr('datainactive'));
        i.addClass('fa-circle-o').removeClass('fa-check-circle');
      }     
  }).on('click','.subirfile',function(ev){
        __subirfile($(this));
  }).on('submit','#frm-'+idgui,function(ev){
        ev.preventDefault();
        var tb=$(this).attr('tb')||'';
        var accion=$(this).attr('accion')||'guardar';
        if(tb=='') return false;
        var datareturn=$(this).attr('datareturn')||'';
        var fele = document.getElementById("frm-"+idgui);
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/'+tb+'/'+accion,
              showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                if(datareturn!='') {
                  md.trigger(datareturn);
                }
                else redir('_back_');
                }
              }
          });
  }).on('click','#btn-cambiarclave',function(ev){
    ev.preventDefault();
    var frm=$(this).closest('form');
    var tb=frm.attr('tb')||'';
    var accion=frm.attr('accion')||'cambiarclave';
    if(tb=='') return false;
    var datareturn=frm.attr('datareturn')||'';
    var usuario=frm.find('#txtusuario').val()||'';
    var clave=frm.find('#txtclave').val()||'';
    var reclave=frm.find('#txtclave2').val()||'';
    var email=frm.find('#txtemail').val()||'';
    if(clave!=reclave || clave==''){
        mostrar_notificacion(frm.find('#txtclave').attr('Attention'),frm.find('#txtclave').attr('error'),"warning");
        frm.find('#txtclave').css({border:'1px solid red'});
        frm.find('#txtclave').focus();
        return;
    }
    var fele = document.getElementById("frm-"+idgui);
    var infoempr=__infoempresa();
    var nomemp=(infoempr.nombreurl!=null&&infoempr.nombreurl!='')?infoempr.nombreurl:infoempr.idproyecto;
    var data=new FormData(fele)
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/'+tb+'/guardarclave',
          showmsjok:true,
          callback:function(rs){
            /*if(rs.code==200){
              var htmlcorreo='Datos de usuario<br>';
              htmlcorreo+='<b>Usuario<b>:'+usuario+'<br>';
              htmlcorreo+='<b>Clave</b>:'+clave+'<br>';
              htmlcorreo+='<b>Link de Acceso a la Plataforma<b><br>';
               htmlcorreo+='<a href="'+_sysUrlBase_+nomemp+'">'+_sysUrlBase_+nomemp+'</a>';
              var fd2 = new FormData();
              fd2.append('mensaje', htmlcorreo);
              fd2.append('paratodos', false);
              fd2.append('asunto', 'Cuenta de Acceso a plataforma');
              fd2.append('paraemail', JSON.stringify([{nombre:usuario,email:email}]));
              __sysAyax({ 
                fromdata:fd2,
                url:_sysUrlBase_+'json/sendemail/enviarcorreoall',
                showmsjok:true,
                callback:function(rs){
                   //mostrar_notificacion("<?php echo JrTexto::_('Attention');?>",data.msj,'success');
                   if(datareturn!='') md.trigger(datareturn);
                   else redir(_sysUrlBase_+tb);
                }
              })
            }*/
          }
      });
  }).on('blur','#txtusuario',function(ev){
    var frm=$(this).closest('form');
    var tb=frm.attr('tb')||'';
    var data=new FormData();
    var dni=$(this).val()||'';
    if(dni=='') return;
    data.append('idpersona', frm.find('#idpersona').val()||''); 
    data.append('usuario',frm.find('#txtusuario').val()||''); 
    data.append('validarusuario',true);
      __sysAyax({
        fromdata:data,
          url:_sysUrlBase_+'json/'+tb+'/listado',
          callback:function(rs){
            if(rs.code==200){
              if(rs.data.length){
                mostrar_notificacion(frm.find('#txtclave').attr('Attention'),frm.find('#txtusuario').attr('error'),"warning");
                frm.find('#txtusuario').css({border:'1px solid red'});
                //frm.find('#txtusuario').focus();
              }else frm.find('#txtusuario').css({border:'1px solid green'});
          }
        }
       });
  }).on('blur','#txtclave',function(ev){
    var frm=$(this).closest('form');
    var clave=frm.find('#txtclave').val()||'';
    var reclave=frm.find('#txtclave2').val()||'';
    if(clave==''){
         //frm.find('#txtclave').focus();
       // mostrar_notificacion(frm.find('#txtclave').attr('Attention'),frm.find('#txtclave').attr('error'),"warning");
        frm.find('#txtclave').css({border:'1px solid red'});
       
        return;
    }else  frm.find('#txtclave').css({border:'1px solid green'});
  }).on('blur','#txtclave2',function(ev){
    var frm=$(this).closest('form');
    var clave=frm.find('#txtclave').val()||'';
    var reclave=frm.find('#txtclave2').val()||'';
    if(clave!=reclave || clave==''){
        //frm.find('#txtclave2').focus();
       // mostrar_notificacion(frm.find('#txtclave').attr('Attention'),frm.find('#txtclave').attr('error'),"warning");
        frm.find('#txtclave2').css({border:'1px solid red'});
        
        return;
    }else  frm.find('#txtclave2').css({border:'1px solid green'});
  }).on('blur','#dni',function(ev){    
    var data=new FormData();
    var dni=$(this).val()||'';
    if(dni=='') return;
    data.append('dni', dni); 
    data.append('sqlsolopersona',true);
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/persona',             
          callback:function(rs){
            if(rs.code==200){
              if(rs.data.length==0) return;
              rw=rs.data[0];
              md.find('#idpersona').val(rw.idpersona);
              md.find('#nombre').val(rw.nombre);
              md.find('#ape_paterno').val(rw.ape_paterno);
              md.find('#ape_materno').val(rw.ape_materno);
              md.find('#sexo').val(rw.sexo);
              md.find('#telefono').val(rw.telefono);
              md.find('#celular').val(rw.celular);
              md.find('#fechanac').val(rw.fechanac);
              md.find('#estado_civil').val(rw.estado_civil||'S');
              var varfoto=rw.foto||_imgdefecto;
              var ifoto=varfoto.lastIndexOf("static/");
              if(ifoto>0) varfoto=_sysUrlBase_+varfoto.substring(ifoto);
              var ifoto=varfoto.lastIndexOf(".");
              if(ifoto<0) varfoto=_sysUrlBase+_imgdefecto;

              md.find('#foto').val(varfoto);                                 
              md.find('#fotofile').attr('src',varfoto);
              md.find('#estado').val(rw.estado); 
              if(rw.estado==1){
                md.find('a#aestado').addClass('active'); 
                md.find('a#aestado').removeClass('active'); 
              }
          }
        }
       });
  }).find('.datetimepicker').each(function(i,v){
    var input=$(v).find('input');
    if(input.length){     
      if(input.hasClass('time'))input.datetimepicker({format:'hh:ii:ss',autoclose:true,pickDate: false});
      else if(input.hasClass('datetime'))input.datetimepicker({format:'yyyy-mm-dd hh:ii:ss',autoclose:true,todayBtn: true});
      else input.datetimepicker({format:'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true,pickTime: false});
    }
  });
  var ventanaroles=md.find('.ventanaroles');
  var _tr=[];
  md.find('#traducciones').children().each(function(i,v){
    _tr[$(v).attr('id')]=$(v).text();
  })
  if(ventanaroles.length){
    var idgui=ventanaroles.attr('idgui');
    var secciones=[];
    secciones[null]='';secciones[0]='';secciones[1]='A';secciones[2]='B';secciones[3]='C';secciones[4]='D';secciones[5]='E';secciones[6]='F';secciones[7]='G';secciones[8]='H';
    var idlocaltmp=ventanaroles.find('input[name="idlocaltmp"]').val();
    var idugeltmp=ventanaroles.find('input[name="idugeltmp"]').val();
    var iddretmp=ventanaroles.find('input[name="iddretmp"]').val();


    ventanaroles.on('click','.btnverrolalumno',function(ev){
    var btn=$(this);
    var tr=$(this).closest('tr');
    var idrol=tr.attr('data-idrol')||-1;
    var idpersona=tr.attr('data-idpersona')||-1;
    if(!$(this).children('i').hasClass('fa-eye')){
      tr.siblings('tr.tralumno'+idpersona).remove();
      $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }else{
      $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
      tr.attr('id','tralumno'+idpersona);
      if(idrol==-1||idpersona==-1) return;
      var formData = new FormData();
        formData.append('idalumno', idpersona);
        sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/acad_matricula/buscarjson',
            type:'json',
            showmsjok : false,
            callback:function(data){
              var dt=data.data;
              if(dt.length>0){
                html='';
                tr.siblings('tr.tralumno'+idpersona).remove();
                $.each(dt,function(i,v){
                  html+='<tr class="tralumno'+idpersona+'" ><td> -- '+v.nombre+' </td><td> <a class="eliminarmatricula" data-padre="tralumno'+idpersona+'" data-idmatricula="'+v.idmatricula+'"><i class="fa fa-trash"></i></a></td></tr>';
                })
                $(html).insertAfter(tr);
              }else{
                if(btn.siblings('.btneliminarrol').length==0)
                $(' <a href="#" class="btn btn-xs btneliminarrol"><i class=" fa fa-trash"></i></a>').insertAfter(btn);
              }
            }
        });
    }
  }).on('click','.btnverroldocente',function(ev){
    var btn=$(this);
    var tr=$(this).closest('tr');
    var idrol=tr.attr('data-idrol')||-1;
    var idpersona=tr.attr('data-idpersona')||-1;
    if(!$(this).children('i').hasClass('fa-eye')){
      tr.siblings('tr.trdocente'+idpersona).remove();
      $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }else{
      $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
      tr.attr('id','trdocente'+idpersona);
      if(idrol==-1||idpersona==-1) return;
      var formData = new FormData();
        formData.append('iddocente', idpersona);
        __sysAyax({
            fromdata:formData,
            url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
            type:'json',
            showmsjok : false,
            callback:function(data){
              var dt=data.data;
              if(dt.length>0){
                html='';
                tr.siblings('tr.trdocente'+idpersona).remove();
                $.each(dt,function(i,v){
                  html+='<tr class="trdocente'+idpersona+'" ><td> -- <b>'+_tr['iiee']+':</b>'+v.strlocal+', <b>'+_tr['curso']+':</b>'+v.strcurso+', <b>'+_tr['grado']+':</b>'+v.idgrado+', <b>'+_tr['seccion']+':</b>'+secciones[v.idsesion]+' </td><td> <a class="eliminarasignaciondocente" data-padre="trdocente'+idpersona+'" data-idgrupoauladetalle="'+v.idgrupoauladetalle+'"><i class="fa fa-trash"></i></a></td></tr>';
                })
                $(html).insertAfter(tr);
              }else{
                if(btn.siblings('.btneliminarrol').length==0)
                $(' <a href="#" class="btn btn-xs btneliminarrol"><i class=" fa fa-trash"></i></a>').insertAfter(btn);
              }
            }
        });
    }
  }).on('click','.btneliminarrol',function(ev){
      ev.preventDefault();
      var btn=$(this);
      var iddetalle=$(this).closest('tr').attr('data-iddetalle')||-1;
      if(iddetalle==-1) return;
      $.confirm({
        title: _tr["confirm_action"]||'Confirm action',
        content: _tr["delete_this_record"]||'It is sure to delete this record ?',
        confirmButton: _tr["accept"]||'Accept', 
        cancelButton: _tr["cancel"]||'Cancel', 
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var formData = new FormData();
          formData.append('iddetalle', iddetalle);
          __sysAyax({
            fromdata:formData,
            url:_sysUrlBase_+'json/persona_rol/eliminar',
            callback:function(rs){
             if(rs.code==200) btn.closest('tr').remove();
            }
          });
          //var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
          //if(res) btn.closest('tr').remove();
        }
      });
    }).on('click','.eliminarasignaciondocente',function(ev){
      var tr=$(this).closest('tr');
      var idgrupoauladetalle=$(this).attr('data-idgrupoauladetalle')||-1;
      var datapadre=$(this).attr('data-padre');
      if(idgrupoauladetalle==-1) return;
      $.confirm({
        title: _tr["confirm_action"]||'Confirm action',
        content: _tr["delete_this_record"]||'It is sure to delete this record ?',
        confirmButton: _tr["accept"]||'Accept', 
        cancelButton: _tr["cancel"]||'Cancel', 
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_grupoauladetalle', 'setcampo', idgrupoauladetalle,'iddocente','0');
          if(res){           
            if(tr.siblings('tr.'+datapadre).length==0){
              var iddetalle=$('tr#'+datapadre).attr('data-iddetalle');
              var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
              if(res)$('tr#'+datapadre).remove();
            }
            tr.remove()
          }
        }
      }) 
    }).on('click','.eliminarmatricula',function(ev){
      var tr=$(this).closest('tr');
      var idmatricula=$(this).attr('data-idmatricula')||-1;
      var datapadre=$(this).attr('data-padre');
      if(idmatricula==-1) return;
      $.confirm({
        title: _tr["confirm_action"]||'Confirm action',
        content: _tr["delete_this_record"]||'It is sure to delete this record ?',
        confirmButton:  _tr["accept"]||'Accept', 
        cancelButton: _tr["cancel"]||'Cancel', 
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'eliminar', idmatricula);
          if(res){
            if(tr.siblings('tr.'+datapadre).length==0){
              var iddetalle=$('tr#'+datapadre).attr('data-iddetalle');
              var res = xajax__('', 'persona_rol', 'eliminar', iddetalle);
              if(res)$('tr#'+datapadre).remove();
            }
            tr.remove()
          }
        }
      });
    }).on('click','.btnagregarrol',function(ev){
      $(this).parent().hide();
      ventanaroles.find('#paneldatosaimportar').removeClass('hide').show();
      ventanaroles.find('table.tableimport').hide(); 
    }).on('change','#_fkcbrol',function(ev){
      var rol=$(this).val()||'';
      if(rol!='1' && rol!='9' && rol!='5'){
        $('._pnldocente').show();
        $('._pnlnodocente').removeClass('col-md-6').addClass('col-md-3');
        if(rol=='7'){       
          $('#_fkcbiiee').closest('._pnldocente').hide();
        }else if(rol=='8'){        
          $('#_fkcbiiee').closest('._pnldocente').hide();
          $('#_fkcbugel').closest('._pnldocente').hide();        
        }
        if(iddretmp!=0){
          $('#_fkcbdre').val(iddretmp);
          $('select#_fkcbdre').trigger('change');
        }
      }else{
        $('._pnldocente').hide();
      }
    }).on('click','.btnsaverolpersona',function(ev){
      btn=$(this);
      var idrol=$('#_fkcbrol').val()||-1;
      var idpersona=$('#idpersonarol_').val()||-1;
      var iddetallerol=$('#iddetallerol_').val()||-1;
      if(idrol==-1||idpersona ==-1) return;
      var formData = new FormData();
          formData.append('idrol', idrol);
          formData.append('idpersona', idpersona);
          formData.append('idempresa', '');
          formData.append('idproyecto', '');
          formData.append('idlocal', $('#_fkcbiiee').val());
          formData.append('idugel', $('select#_fkcbugel').val());
        __sysAyax({
            fromdata:formData,
            url:_sysUrlBase_+'json/persona_rol/guardar',
            callback:function(data){
              btn.closest('.modal').modal('hide');
              setTimeout(function(){btn.closest('.modal').remove(); },2500);
            }
        });
    })
  }
}

function frm_personaimport(md){
  md.find('.modal-dialog').removeClass('modal-lg').addClass('modal-xl');
  var idgui=md.find('table.formventana').attr('idgui');
  md.on('click','.btnimport'+idgui,function(ev){
    var clone = document.getElementById('importjson').cloneNode();
      clone.value = '';
      md.find('#contenfile').html(clone);
      md.find('#importjson').trigger('click');
  })

  md.on('click','.btnsaveimport'+idgui,function(ev){
    var datareturn=$('#datosimport'+idgui+' table').attr('datareturn')||'';
    var tableimport=md.find('table.tableimport'+idgui);
    if(tableimport.length){
      var tr=tableimport.find("tr.addregistroimporttr");
      var datosimportados=[];
      $.each(tr,function(i,v){
        var data={
          tipodoc:1, 
          dni:$(v).find("td:eq(0)").text().trim(),
          ape_paterno:$(v).find("td:eq(1)").text().trim(),
          ape_materno:$(v).find("td:eq(2)").text().trim(),
          nombre:$(v).find("td:eq(3)").text().trim(),
          fechanac:$(v).find("td:eq(4)").text().trim(),
          sexo:$(v).find("td:eq(5)").text().trim(),
          estado_civil:'S',
          ubigeo:'',
          urbanizacion:'',
          direccion:'',
          telefono:'',
          celular:$(v).find("td:eq(6)").text().trim(),
          email:$(v).find("td:eq(7)").text().trim(),
          usuario:$(v).find("td:eq(8)").text().trim(),
          clave:$(v).find("td:eq(9)").text().trim(),
          idrol:md.attr('idrol')||3,
          foto:'',
          estado:1,
          situacion:1,
          idugel:md.attr('idugel')||0,         
          idiiee:md.attr('idlocal')||0,         
          idioma:'EN'
        }
        datosimportados.push(data);
      });
     
      var formData = new FormData();
      formData.append('datajson', JSON.stringify(datosimportados));
      formData.append('idrol', md.attr('idrol')||3);
      formData.append('idugel', md.attr('idlocal')||0);
      formData.append('idlocal', md.attr('idugel')||0);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'json/persona/importar',
        showmsjok : false,
        callback:function(dt){
          if(dt.code==200){
            var data=dt.data;
            var hayerror=0;
            var idpersonas='';
            $.each(data,function(i,v){
              if(v.msj==200){
                tableimport.find('td[dni="'+v.dni+'"]').addClass('tdcorrecto').closest('tr').addClass('bg-success').removeClass('bg-warning');
                idpersonas+=v.idpersona+',';
              }else{
                hayerror++;
                tableimport.find('td[dni="'+v.dni+'"]').addClass('tderror').closest('tr').removeClass('bg-success').addClass('bg-warning');
              }
            })
            /*if(hayerror>0){

            }*/
            if(datareturn!=''){
              md.attr('idpersonas',idpersonas);
              md.trigger(datareturn);
            }else redir(_sysUrlBase_+'persona');
          }
        }
      }
      __sysAyax(data);
      return false;
    }    
  }).on('click','.btn_removetr',function(ev){
      $(this).closest('tr').remove();
  }).on('change','#importjson',function(ev){
    var table=md.find('#datosimport'+idgui);
    var donde=md.find('#paneldatosaimportar'+idgui);
    do_file(ev.target.files,{table:table,hoja:1,col:4,fil:4,donde:donde});
    md.find('#pnlbotonesde'+idgui).hide();
    md.find('#paneldatosaimportar'+idgui).show();
  }).on('click','.btncancel',function(ev){
    //var datareturn=$('#datosimport'+idgui+' table').attr('datareturn')||'';
    //if(datareturn!='') md.trigger(datareturn);
  //else redir(_sysUrlSitio_+'/min_dre');
    md.find('#pnlbotonesde'+idgui).show();
    md.find('#paneldatosaimportar'+idgui).hide();
  })
  md.find('.hide').hide();
}