function frm(md){
  //var id_ubigeo=md.find('#txtUbigeo').attr('datavalue');
  var _imgdefecto='static/media/usuarios/user_avatar.jpg';
  var idgui=md.find('form').attr('idgui'); 
  _busdni=function(dt){
    var data=new FormData();
    var dni=dt.dni||'';
    var idpersona=dt.idpersona||'';
    if(dni=='' && idpersona=='' ) return;
    if(dni!='')data.append('dni', dni); 
    else  data.append('idpersona', idpersona); 
    data.append('sqlsolopersona',true);
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/persona',             
          callback:function(rs){
            if(rs.code==200){
              if(rs.data.length==0) return;
              rw=rs.data[0];
              md.find('#idpersona').val(rw.idpersona);
              md.find('#dni').val(rw.dni);
              md.find('#personanombre').val(rw.ape_paterno+' '+rw.ape_materno+', '+rw.nombre);           
          }
        }
       });
  }
  var idpersona=md.find('#idpersona').val();
  if(idpersona!='')_busdni({idpersona:$('#idpersona').val()})
  md.on('submit','#frm-'+idgui,function(ev){
        ev.preventDefault();
        var idcurso=md.find('#idcurso').val()||'';
        if(idcurso=='') return __notificar({title:'',html:md.find('#idcurso').attr('title'),type:'warning'});
        var tb=$(this).attr('tb')||'';
        var accion=$(this).attr('accion')||'guardar';
        if(tb=='') return false;
        var datareturn=$(this).attr('datareturn')||'';
        var fele = document.getElementById("frm-"+idgui);
        var data=new FormData(fele)
        data.append('idgrado',md.find('select#idgrupoaula option:selected').attr('idgrado')||0);
        data.append('idsesion',md.find('select#idgrupoaula option:selected').attr('idseccion')||0);
        var vcursos=[];
        if(idcurso.length>1){         
          vcursos=JSON.stringify(idcurso)   
          data.append('varioscursos',true);
        }else vcursos=idcurso[0];
        data.append('idcurso',vcursos);
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/'+tb+'/'+accion,
              showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                if(datareturn!='') md.trigger(datareturn);
                else redir(_sysUrlBase_+tb);
                }
              }
          });  
  }).on('blur','#dni',function(ev){
    ev.preventDefault();

    _this=$(this); 
    return _busdni({'dni':_this.val()});     
  }).on('keydown','#dni',function(ev){
   // ev.preventDefault();
    if(ev.keyCode===13){
      _this=$(this);         
       _this.trigger('blur');
    }
  })

  var idlocal=md.find('select#idlocal').attr('idlocal')||-1;
  var idambiente=md.find('select#idambiente').attr('idambiente')||-1;
  var idgrado=md.find('select#idgrupoaula option:selected').attr('idgrado')||0;
  var idsesion=md.find('select#idgrupoaula option:selected').attr('idseccion')||0;
  __busdre=function(){
    var fd = new FormData();
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/min_dre',
      callback:function(rs){
        $sel=md.find('#iddre');
        iddre=$sel.attr('iddre')||'';
        dt=rs.data;
        $.each(dt,function(i,v){
          $sel.append('<option value="'+v.iddre+'" '+(v.iddre==iddre?'selected="selected"':'')+ ' dataubigueo="'+v.ubigeo+'" >'+v.iddre +" : "+ v.descripcion+'</option>');
        })
        $sel.trigger('change');
      }
    })
  } 
   __busdre(); 
  md.on('change','#iddre',function(ev){
    $sel=md.find('#idugel');
    var fd = new FormData();
     fd.append('iddre',$(this).val());
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ugel',
      callback:function(rs){
        $sel.children('option').eq(0).siblings().remove();
          idsel=$sel.attr('idugel')||'';
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.idugel+'" '+(v.idugel==idsel?'selected="selected"':'')+ ' dataubigueo="'+v.id_ubigeo+'" >'+v.idugel +" : "+ v.descripcion+'</option>');
          })
          $sel.trigger('change');
      }
    })
  }).on('change','#idugel',function(ev){
    $sel=md.find('#idlocal');
    var fd = new FormData();
     fd.append('idugel',$(this).val());
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/local',
      callback:function(rs){
        $sel.children('option').eq(0).siblings().remove();
          idsel=$sel.attr('idlocal')||'';
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.idlocal+'" '+(v.idlocal==idsel?'selected="selected"':'')+ ' >'+v.idlocal +" : "+ v.nombre+'</option>');
          })
          $sel.trigger('change');
      }
    })
  }).on('change','#idlocal',function(ev){
    $sel=md.find('#idambiente');
    var fd = new FormData();
     fd.append('idlocal',$(this).val());
    __sysAyax({
      fromdata:fd,
      url:_sysUrlBase_+'json/ambiente',
      callback:function(rs){
        $sel.children('option').eq(0).siblings().remove();
          idsel=$sel.attr('idambiente')||'';
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.idambiente+'" '+(v.idambiente==idsel?'selected="selected"':'')+ ' >'+v.idambiente +" : "+ v.tipo+' '+v.numero+'</option>');
          })
          //$sel.trigger('change');
      }
    })
  }).on('click','.btnbuscarteacher',function(ev){
     ev.preventDefault();
     var _this=$(this); 
      var url=_sysUrlBase_+'personal/buscar/?plt=modal&idrol=2';   
      var titulo=$(this).attr('data-titulo')||'';
      var _md=__sysmodal({'url':url,'titulo':titulo});
      var refresh='refresh'+__idgui();
      //_this.addClass(refresh);
      _md.on(refresh, function(ev){
        var _dr=_this.parent();      
        var id=_md.attr('idpersona');
        _dr.find('#idpersona').val(id);
        var dni=_md.attr('dni');
        _dr.find('#dni').val(dni);
        var nombre=_md.attr('nombre');
        _dr.find('#personanombre').val(nombre);
        _md.trigger('cerramodal');
      })
      _md.on('shown.bs.modal',function(){
        var formventana=_md.find('.formventana');
        formventana.attr('datareturn',refresh).addClass('ismodaljs');
          if(_md.find('#cargarscript').length)
          $.getScript(_md.find('#cargarscript').attr('src'),function(){frmpersonabuscar(_md);});
      })
  })
  md.find('.datetimepicker').each(function(i,v){
    var input=$(v).find('input');
    if(input.length){     
      if(input.hasClass('time'))input.datetimepicker({format:'hh:ii:ss',autoclose:true,pickDate: false});
      else if(input.hasClass('datetime'))input.datetimepicker({format:'yyyy-mm-dd hh:ii:ss',autoclose:true,todayBtn: true});
      else input.datetimepicker({format:'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true,pickTime: false});
    }
  })
  md.find('.selectpicker').selectpicker();
  __infoempresa();
}