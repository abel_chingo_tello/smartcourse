function frm_ambiente(md){
  //var id_ubigeo=md.find('#txtUbigeo').attr('datavalue');
  var idgui=md.find('form').attr('idgui'); 
  md.on('click','.btn-activar',function(){
      var i=$("i",this);  
      if(i.hasClass('fa-circle-o')){
        $(this).addClass('active');
        $('input',this).val(1);
        $('span',this).text($(this).attr('dataactive'));
        i.removeClass('fa-circle-o').addClass('fa-check-circle');
      }else{
        $(this).removeClass('active');
        $('input',this).val(0);
        $('span',this).text($(this).attr('datainactive'));
        i.addClass('fa-circle-o').removeClass('fa-check-circle');
      }     
  }).on('click','.subirfile',function(ev){
        __subirfile($(this));
  }).on('submit','#frm-'+idgui,function(ev){
        ev.preventDefault();
        var datareturn=$(this).attr('datareturn')||'';
        var fele = document.getElementById("frm-"+idgui);
        var data=new FormData(fele)
          __sysAyax({ 
            fromdata:data,
              url:_sysUrlBase_+'json/ambiente/guardar',
              showmsjok:true,
              callback:function(rs){
                if(rs.code==200){
                if(datareturn!='') md.trigger(datareturn);
                else redir(_sysUrlBase_+'ambiente');
            }
          }
      });
  })
  __infoempresa();
}