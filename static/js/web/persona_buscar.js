function frmpersonabuscar(md){
  var _imgdefecto='static/media/usuarios/user_avatar.jpg';
  //var idgui=md.find('form').attr('idgui'); 
  var acc=md.find('.formventana').attr('accion');  
  if(acc=='listado'){
    var ventana=md.find('.formventana');
    datareturn=ventana.attr('datareturn');
    var tbpersonas=ventana.find('.table').DataTable(
    { "searching": false,
      "processing": false,     
      "ajax":{
        url:_sysUrlBase_+'json/persona',
        type: "post",                
        data:function(d){
            d.json=true ;
            d.sexo=ventana.find('#sexo').val();
            d.estado_civil=ventana.find('#estado_civil').val();
            d.estado=ventana.find('#estado').val();            
            d.rol=ventana.find('#idrol').val();
            d.idproyecto=ventana.find('#idlocal').val();
            d.texto=ventana.find('input.textosearchlist').val();
            draw_=d.draw;
           // console.log(d);
        },
        dataSrc:function(json){
          var data=json.data;             
          json.draw = draw_;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var varfoto=data[i].foto||_imgdefecto;
            var ifoto=varfoto.lastIndexOf("static/");
            if(ifoto>=0) varfoto=_sysUrlBase_+varfoto.substring(ifoto);
            if(ifoto==-1){
              var ifoto=varfoto.lastIndexOf("/");
              var ifoto2=varfoto.lastIndexOf(".");
              if(ifoto>=0 && ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto.substring(ifoto);
              else if(ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto;
              else varfoto=_sysUrlBase_+_imgdefecto;
            }
            varfoto='<img src="'+varfoto+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">';
            datainfo.push([
              (i+1),
              (data[i].dni+'<br>'+data[i].ape_paterno+' '+data[i].ape_materno+','+data[i].nombre),
              data[i].sexo,(data[i].telefono +'<br>'+data[i].celular),
              data[i].email,
              data[i].usuario,
              varfoto,
              '<a href="javascript:;"  class="btn-activar  '+(data[i].estado==0?'':'active')+' " data-campo="estado"  data-id="'+data[i].idpersona+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> </a>',
              '<a href="javascript:;" alt="seleccionar"  class="btn-seleccionar " data-id="'+data[i].idpersona+'" data-dni="'+data[i].dni+'" data-nombre="'+data[i].ape_paterno+' '+data[i].ape_materno+','+data[i].nombre+'"> <i class="fa fa-hand-pointer-o"></i> </a>'
              ]);
            }
          return datainfo }, error: function(d){console.log(d)}
      },"language": { "url": _sysIdioma_=='es'?_sysUrlStatic_+"/libs/datatable1.10/idiomas/es.json":''}
    });;
    ventana.on('change','#sexo',function(ev){tbpersonas.ajax.reload();
    }).on('change','#estado_civil',function(ev){tbpersonas.ajax.reload();
    }).on('change','#estado',function(ev){tbpersonas.ajax.reload();
    }).on('click','.btnbuscar',function(ev){tbpersonas.ajax.reload();
    }).on('keydown','.textosearchlist',function(ev){if(ev.keyCode===13) tbpersonas.ajax.reload();
    }).on('blur','.textosearchlist',function(ev){tbpersonas.ajax.reload();
    }).on('click','.btn-activar',function(){
      _this=$(this);
      var activo=1;
      if(_this.hasClass('active')){ activo=0;}
      var data=new FormData();
      data.append('idpersona',_this.attr('data-id'));
      data.append('campo',_this.attr('data-campo'));
      data.append('valor',activo);
      __sysAyax({ 
        fromdata:data,
          url:_sysUrlBase_+'json/persona/setcampo',
          showmsjok:true,
          callback:function(rs){
            if(rs.code==200){
              tbpersonas.ajax.reload();
            }
          }
      });
    }).on('click','.btn-seleccionar',function(){
      var id=$(this).attr('data-id');
      var dni=$(this).attr('data-dni');
      var nombre=$(this).attr('data-nombre');
      md.attr('idpersona',id).attr('dni',dni).attr('nombre',nombre);
      md.trigger(datareturn);
    }).on('click','.btnagregarpersona',function(ev){
      ev.preventDefault();
      var _this=$(this); 
      var url=_this.attr('href');
      if(url.indexOf('?')!=-1) url+='&plt=modal';
      else url+='?plt=modal';
      url=url+'&idrol='+$('input#idrol').val()+'&idproyecto='+$('input#idproyecto').val();
      var titulo=$(this).attr('data-titulo')||'';
      var _md=__sysmodal({'url':url,'titulo':titulo});
      var refresh='refresh'+__idgui();
      //_this.addClass(refresh);
      _md.on(refresh, function(ev){
        var id=$(this).attr('data-id');
        var dni=$(this).attr('data-dni');
        var nombre=$(this).attr('data-nombre');
        md.attr('idpersona',id).attr('dni',dni).attr('nombre',nombre);  
        _md.trigger('cerramodal');
        md.trigger(datareturn);
      })
      _md.on('shown.bs.modal',function(){
        var formventana=_md.find('.formventana');
        formventana.attr('datareturn',refresh).addClass('ismodaljs');
         // if(_md.find('#cargarscript').length)
         // $.getScript(_md.find('#cargarscript').attr('src'),function(){});
          frmpersonabuscar(_md);
      })
    })
  }else if(acc=='adicionar'){
    var idgui=md.find('form').attr('idgui');
    md.on('click','.btn-activar',function(){
        var i=$("i",this);  
        if(i.hasClass('fa-circle-o')){
          $(this).addClass('active');
          $('input',this).val(1);
          $('span',this).text($(this).attr('dataactive'));
          i.removeClass('fa-circle-o').addClass('fa-check-circle');
        }else{
          $(this).removeClass('active');
          $('input',this).val(0);
          $('span',this).text($(this).attr('datainactive'));
          i.addClass('fa-circle-o').removeClass('fa-check-circle');
        }     
    }).on('click','.subirfile',function(ev){
          __subirfile($(this));
    }).on('submit','#frm-'+idgui,function(ev){
      ev.preventDefault();
      var tb=$(this).attr('tb')||'';
      var accion='guardar';
      if(tb=='') return false;
      var datareturn=$(this).attr('datareturn')||'';
      var fele = document.getElementById("frm-"+idgui);
      var data=new FormData(fele)
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/'+tb+'/'+accion,
            showmsjok:false,
            callback:function(rs){
              if(rs.code==200){
              if(datareturn!='') {
                var id=rs.newid;
                var dni=md.find('input#dni').val();
                var nombre=md.find('input#ape_paterno').val()+' '+md.find('input#ape_materno').val()+' '+md.find('input#nombre').val();
                md.attr('data-id',id).attr('data-dni',dni).attr('data-nombre',nombre);
                md.trigger(datareturn);
              }else redir(_sysUrlBase_+tb);
              }
            }
        });
    }).on('blur','#dni',function(ev){    
      var data=new FormData();
      var dni=$(this).val()||'';
      if(dni=='') return;
      data.append('dni', dni); 
      data.append('sqlsolopersona',true);
        __sysAyax({ 
          fromdata:data,
            url:_sysUrlBase_+'json/persona',             
            callback:function(rs){
              if(rs.code==200){
                if(rs.data.length==0) return;
                rw=rs.data[0];
                md.find('#idpersona').val(rw.idpersona);
                md.find('#nombre').val(rw.nombre);
                md.find('#ape_paterno').val(rw.ape_paterno);
                md.find('#ape_materno').val(rw.ape_materno);
                md.find('#sexo').val(rw.sexo);
                md.find('#telefono').val(rw.telefono);
                md.find('#celular').val(rw.celular);
                md.find('#fechanac').val(rw.fechanac);
                md.find('#estado_civil').val(rw.estado_civil||'S');
                md.find('#foto').val(rw.foto||'');
                var varfoto=rw.foto||_imgdefecto;
                var ifoto=varfoto.lastIndexOf("static/");
                if(ifoto>=0) varfoto=_sysUrlBase_+varfoto.substring(ifoto);
                if(ifoto==-1){
                  var ifoto=varfoto.lastIndexOf("/");
                  var ifoto2=varfoto.lastIndexOf(".");
                  if(ifoto>=0 && ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto.substring(ifoto);
                  else if(ifoto2>=0) varfoto=_sysUrlBase_+'static/media/usuarios/'+varfoto;
                  else varfoto=_sysUrlBase_+_imgdefecto;
                }              
                md.find('#fotofile').attr('src',varfoto);
                md.find('#estado').val(rw.estado); 
                if(rw.estado==1){
                  md.find('a#aestado').addClass('active'); 
                  md.find('a#aestado').removeClass('active'); 
                }
            }
          }
         });
    }).find('.datetimepicker').each(function(i,v){
      var input=$(v).find('input');
      if(input.length){     
        if(input.hasClass('time'))input.datetimepicker({format:'hh:ii:ss',autoclose:true,pickDate: false});
        else if(input.hasClass('datetime'))input.datetimepicker({format:'yyyy-mm-dd hh:ii:ss',autoclose:true,todayBtn: true});
        else input.datetimepicker({format:'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true,pickTime: false});
      }
    });
  }
}