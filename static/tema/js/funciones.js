
var nom_dia_semana = Array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
var _sysrdtjson='';
var _logobase64='';
var modal_id = 0;
function __extend(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}
function redir(pg){
	if(pg==undefined||pg=='')window.location.href=window.location;
	else {
		if(pg=='_back_'){
			var prevUrl = document.referrer;
			if(prevUrl.indexOf(window.location.host) !== -1) { window.history.back();}
		}else window.location.href=pg;
	}
}

$.fn.extend({
    ismoneda: function(){
    	$(this).on('keypress',function(ev){
    		var keynum = window.event ? window.event.keyCode : event.which;
			if(true == swi_keys_neu.existe(keynum)) { return true;}
			return /\d/.test(String.fromCharCode(keynum));
    	})        
    },
    isentero:function(){
    	$(this).on('keypress',function(ev){
    		var keynum = window.event ? window.event.keyCode : event.which;
			if(true == swi_keys_int.existe(keynum)) { return true;}
			return /\d/.test(String.fromCharCode(keynum));
    	}) 
    },
    isdate:function(){
    	if( typeof datepicker !== 'undefined' && jQuery.isFunction(datepicker) ) {
			$(this).datepicker({'format' : 'yyyy-mm-dd'});
			$(this).on('click',function(ev){
				$(this).datepicker({autoclose: true});
				$(this).datepicker('show');
			}).on('keydown',function(event){
				if(9 == event.keyCode) {
					$(this).datepicker('hide');
				}
			});
		}
    },
    enmoneda:function(value,decimales){
    	decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
		separators = [',', "'", '.'];
		var number = (parseFloat(value) || 0).toFixed(decimals);
	    if (number.length <= (4 + decimals))
	        return number.replace('.', separators[separators.length - 1]);
	    var parts = number.split(/[-.]/);
	    value = parts[parts.length > 1 ? parts.length - 2 : 0];
	    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
	        separators[separators.length - 1] + parts[parts.length - 1] : '');
	    var start = value.length - 6;
	    var idx = 0;
	    while (start > -3) {
	        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
	            + separators[idx] + result;
	        idx = (++idx) % 2;
	        start -= 3;
	    }
	    return (parts.length == 3 ? '-' : '') + result;
    }
});



$(document).ready(function(){
		
});

function agregar_msj_interno(tipo, mensaje) {
	html = '<div class="alert alert-' + tipo + ' alert-dismissible fade show" role="alert">'
			+ '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'
			+ '<span class="sr-only">Error: </span>' + mensaje
			+ '</div>';	
	$('#msj-interno').empty().html(html);
}
function agregar_alert(obj){
	var donde=obj.d||$('#msj-interno');
	var tipo=obj.tipo||'success';
	if(tipo=='Error') tipo='danger';
	var titulo = obj.t||'';
	if(titulo!='') titulo='<h4 class="alert-heading">'+titulo+'!</h4><hr>';
	var mensaje=(obj.m||'')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	var html='<div class="alert alert-'+tipo+' alert-dismissible fade show" role="alert">'+titulo+mensaje+'</div>';
	var limpiar=obj.c==false?false:true;
	if (limpiar)donde.html(html);
	else donde.append(html);
}


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    },
    getBrowserInfo :function(){
	    var ua= navigator.userAgent, tem, 
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	    if(/trident/i.test(M[1])){
	        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	        return 'IE '+(tem[1] || '');
	    }
	    if(M[1]=== 'Chrome'){
	        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	    }
	    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	    return {navegador:M[0],version:M[1]};
	}
};

function diff_horas(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(hora_i);
	h_f = pasar_hora_a_horas(hora_f);	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	}else{
		horas = 24 - h_i + h_f;
	}	
	return horas;
}

function pasar_hora_a_horas(hora) {
	hora = hora.split(':');
	horas = parseInt(hora[0]);
	horas += Math.abs((parseInt(hora[1])/60));	
	return horas;
}

function dia_semana(fecha) {
	var dia = new Date(fecha);	
	return nom_dia_semana[dia.getDay()];
}

function smkGMap(lat, long, zoom, ele) {
	var mapOptions = {
    	zoom: zoom,
		center: new google.maps.LatLng(lat, long),
		panControl: true,
		zoomControl: true,
		scaleControl: true,
		streetViewControl: false
	}	
	var mapa = new google.maps.Map(document.getElementById(ele), mapOptions);  	
	return mapa;
}

function smkGMapMarca_add(mapa, posicion, titulo, editable, ir) {
	//var imagen = 'images/beachflag.png';
	var marker = new google.maps.Marker({
		position: posicion,
		map: mapa,
		//icon: imagen,
		draggable:editable,
    	title: titulo
	});
	
	if(true == ir) {
		smkGMapPos_centrar(mapa, posicion);
	}
	
	/*google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(mapa, marker);
	});*/
	
	//marker.setMap(null);
	
	return marker;
}

function smkGMapPos_centrar(mapa, posicion){mapa.setCenter(posicion);}
function smkGMapPos_crear(lat, long) { var pos = new google.maps.LatLng(lat, long);	return pos;}
function genHorasDia(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;}

function genHorasDiaInc(hora_inicio, espacio_horas){
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];	
	if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
		horas.push('00:' + hora_inicio.format('mmA'));
	} else {
		horas.push(hora_inicio.format('hh:mmA'));
	}	
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;}
function formatoHora12(time){return time.match(/^(0?[0-9]|1[012])(:[0-5]\d)[APap][mM]$/); }
function dias_entre_fechas(fecha_i, fecha_f) {//01.05.15
	fecha_i = Date.UTC(fecha_i.getFullYear(), fecha_i.getMonth(), fecha_i.getDate(), fecha_i.getHours(), fecha_i.getMinutes());
	fecha_f = Date.UTC(fecha_f.getFullYear(), fecha_f.getMonth(), fecha_f.getDate(), fecha_f.getHours(), fecha_f.getMinutes());
	var ms = Math.abs(fecha_i - fecha_f);
	return Math.floor(ms/1000/60/60/24);}

function isNumber(n) {
    n = n.replace(/\./g, '').replace(',', '.');
    return !isNaN(parseFloat(n)) && isFinite(n);}
modal_id = 0;
function openModal(tam,titulo,url, destruir,clase,form){
    var _newmodal = $('#modalclone').clone(true,true);
    var form=form||{header:true,footer:true,borrarmodal:false};
    var showheader=form.header!=undefined?form.header:true;
    var showfooter=form.footer!=undefined?form.footer:true;
    var borrarmodal=form.borrarmodal!=undefined?form.borrarmodal:false;
    var tmpurl=form.url!=undefined?form.url:'';
    var anchomodal=form.tam!=undefined?form.tam:'';
    var callback=form.callback||undefined;
    var urladd=url||tmpurl;
    var frmtype=form.method||'GET';
    var frmdata=form.datasend||'';
    var tam=tam||'lg';
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(clase);
    _newmodal.children('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam).css('width',anchomodal);    
    _newmodal.modal({backdrop: 'static', keyboard: true});
    if(true == destruir)
    $('body').on('click', '#modal-' + modal_id + ' .close , #modal-' + modal_id + ' .cerramodal' , function (ev){ 
	    var audio=$(_newmodal.find('audio'));
	    	if(audio.length) audio.trigger('pause');
	    var video=$(_newmodal.find('video'));
	    	if(video.length) video.trigger('pause');
		_newmodal.modal('hide');
		$('body').removeAttr('style');
	if(borrarmodal)_newmodal.on('hidden.bs.modal', function(){_newmodal.remove();});
    });
    ++modal_id; 
    if(urladd){
		$.ajax({
			url: urladd,
			type: frmtype,
			data:  frmdata,
			contentType: false,
			processData: false,
			cache: false,
			dataType:'html',
			beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
			success: function(data)
			{	_newmodal.find('#modaltitle').html(titulo);
	            _newmodal.find('#modalcontent').html(data);
	            if(!showheader) _newmodal.find('.modal-header').hide();
				if(!showfooter) _newmodal.find('.modal-footer').hide();
				if(__isFunction(callback)){callback(_newmodal)};
			},error: function(e){ console.log(e); },
			complete: function(xhr){ }
	    });
	}else{
		if(__isFunction(callback)){callback(_newmodal)};
	}	
    return true;
} 

var selectedfile=function(e,obj,txt,fcall){
  var tipo= $(obj).attr('data-tipo');
  var donde= $(obj).attr('data-url');
  var fcall='&fcall='+fcall;
  if(tipo==''||tipo==undefined)return false;
  var rutabiblioteca= _sysUrlBase_+'/biblioteca/?plt=modal&robj=afile&donde='+donde+'&type='+tipo+fcall;
  var data={
    titulo:txt+' - '+tipo,
    url:rutabiblioteca,
    ventanaid:'biblioteca-'+tipo,
    borrar:true
  }
  var modal=sysmodal(data);
}

var cerrarmodal=function(){   //cerrar ventana de seleccionar file
  $('.btncloseaddinfotxt').trigger('click');
  $('.addinfotitle').html('');
}
	
var sysmodal=function(obj){
    var tam=obj.tam||'lg';
    var htmlid=$(obj.html).html()||'';
    var htmltxt=obj.htmltxt||'';
    var url=obj.url||'';
    var borrar=obj.borrar||false;
    var titulo=obj.titulo||false;
    var claseid=obj.ventanaid||'';
    var cerrarconesc=obj.cerrarconesc||true; //cerrar modal con la tecla escape
    var backdrop=obj.backdrop||'static'; //true : con fondo oscuro, false fondo transaparente //static  no se cierra al hacer click fuera del modal
    var html=htmltxt||htmlid;
    var showfooter=obj.showfooter||false;

    if($('.modal').hasClass(claseid)){
    	$('.modal.'+claseid).modal('show');
    	return false;    	
    }
    var _newmodal = $('#modalclone').clone(true,true);
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(claseid);
    _newmodal.find('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam);
    _newmodal.find('#modaltitle').html(titulo);
    $('body').on('click', '#modal-' + modal_id + ' .cerrarmodal' , function (ev){ 
    	    var audio=$(_newmodal.find('audio'));
    	    	if(audio.length) audio.trigger('pause');
    	    var video=$(_newmodal.find('video'));
    	    	if(video.length) video.trigger('pause');
    		_newmodal.modal('hide');
    		$('body').removeAttr('style'); 
    		if(borrar){
				_newmodal.on('hidden.bs.modal', function () {
					$(this).remove();
				});    			
			}
    });
    
    ++modal_id;
    if(!titulo) _newmodal.find('.modal-header').remove();
    if(!showfooter) _newmodal.find('.modal-footer').remove();
    var donde=_newmodal.find('#modalcontent');
    var modalresize=function(){
        resizemodal($(_newmodal),-20);
    }
    var rdata=sysaddhtml(donde,url,html,modalresize);    
    _newmodal.modal({backdrop: backdrop, keyboard: cerrarconesc});
     //alto maximo del modal    
    return _newmodal;
}

var sysaddhtml=function(donde,url,html,fn){
	if(url){
        donde.html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
        $.get(url, function(data) {            
            $(donde).html(data);
            if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
        });
    }else if(html){
    	 $(donde).html(html);
    	 if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
    }
   
}

var resizemodal = function(modal,ntam) {
  var body, bodypaddings, header, headerheight, height, modalheight;
  ntam=ntam||85;
  header = $(".modal-header", modal);
  footer = $(".modal-footer", modal);
  body = $(".modal-body", modal);
  modalheight = parseInt(modal.css("height"));
  headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
  footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
  bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
  height = modalheight - headerheight - footerheight - bodypaddings - ntam;
  return body.css("max-height", "" + height + "px");
};



function _sysisFile(url){
	try{
	  url=url.substr(1 + url.lastIndexOf("/"));
	 // console.log(url);
  	index=url.indexOf('?');
  	if(index>-1) url=url.substr(0,index);
  	if(url==''||url=='undefined'||url==undefined) return false;
  	indexpunto=url.lastIndexOf(".");
	  if(indexpunto==-1) return false;
	}catch(ex){}
  	return true;  
}

function _sysfileExists(url){
	try{
		if(!_sysisFile(url)) return false;
		var http = new XMLHttpRequest();
		http.open('HEAD', url, true);
		http.send();
		if(http.status!=404) return true;
		return false;
	}catch(ex){}
}
function _isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
var animacssin=["bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","flip","flipInX","flipInY","lightSpeedIn","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","slideInUp","slideInDown","slideInLeft","slideInRight","zoomIn","zoomInDown","zoomInLeft","zoomInRight","zoomInUp","jackInTheBox","rollIn"]; //,"hinge"
var animacssout=["bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flipOutX","flipOutY","lightSpeedOut","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideOutUp","slideOutDown","slideOutLeft","slideOutRight","zoomOut","zoomOutDown","zoomOutLeft","zoomOutRight","zoomOutUp","rollOut"];
var animaccsenf=["bounce","flash","pulse","rubberBand","shake","swing","tada","wobble","jello"];

$.fn.extend({
    imprimir: function() {
		var cloned = this.clone();
        var printSection = $('#printSection');
        if (printSection.length == 0) {
            printSection = $('<div id="printSection"></div>')
            $('body').append(printSection);
		}
        printSection.html(cloned.html());
        var toggleBody = $('body *:visible');
        toggleBody.hide();
        $('#printSection, #printSection *').show();
        window.print();
        printSection.remove();
        toggleBody.show();
	},
    animateCss: function (animationName, callback){
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            if (callback) {
              callback();
            }
        });
        return this;
    },
	subirfile:function(callback){		
		$(this).on('click',function(ev){
			__subirfile($(this),callback);
		})
	},
	cargando:function(infodata){

	},
	sysajax:function(infodata){
		var datasend=infodata.fromdata||'';
		var url=infodata.url||false;
		var type=infodata.type||'json';
		var callback=infodata.callback||false;
		var callbackerror=infodata.callbackerror||false;
		var method=infodata.method||'POST';
		var msjatencion=infodata.msjatencion||'';
		var showmsjok=infodata.showmsjok||false;
		if(!url) return;
		$.ajax({
	      url: url,
	      type: method,
	      data:  datasend,
	      contentType: false,
	      processData: false,
	      cache: false,
	      dataType:type,
	      beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
	      success: function(data)
	      {  
	        
	        if(type=='json'){
		        if(data.code=='Error'){
		          __notificar({title:msjatencion,html:data.msj,type:'warning'});
		          if(__isFunction(callbackerror))callback(data);
		        }else{
		          if(showmsjok) __notificar({title:msjatencion,html:data.msj,type:'success'});
		          if(__isFunction(callback))callback(data);
		        }
		    }else{
		    	if(__isFunction(callback))callback(data);
		    }
	      },
	      error: function(e){ console.log(e); },
	      complete: function(xhr){}
	    });
	},	
	traducir: function(){
    	var $sectionMensajes = $(this);
		$sectionMensajes.find('input').each(function() {
			var id = $(this).attr('id');
			var valor = $(this).val();
			MSJES_PHP[id] = valor;
		});
	}
})


var __isFunction=function(o){ if (typeof o!= 'function') return false; else return true;}
var __idgui=function(){	return Date.now()+(Math.floor(Math.random() * (1 - 100)) + 1);}
var __isDom=function(e){ try{ if(e.length) return true; else return false;}catch(er){return false}}
var __isEmpty=function(e){return !$.trim(e.html());}
var __isJson=function(s){try{JSON.parse(s);}catch(e){return false;}return true;}
var __esURLValida=function(url){
	var pattern = new RegExp('^(https:\\/\\/)?'+ /* protocol */
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ /* domain name */
		'((\\d{1,3}\\.){3}\\d{1,3}))'+ /* OR ip (v4) address */
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ /* port and path */
		'(\\?[;&a-z\\d%_.~+=-]*)?'+ /* query string */
		'(\\#[-a-z\\d_]*)?$','i'); /* fragment locater */
	if(!pattern.test(url)) {
		alert("Please enter a valid URL with HTTPS security protocol.");
		return false;
	} else {
		return true;
	}}
var __subirfile=function(obj,fn){
	
        obj1=__isDom(obj)?obj:obj.file;
		var opt={file:obj1||'',nombre:obj1.attr('nombreguardar')||'',typefile:obj1.attr('typefile')||'imagen',guardar:obj1.attr('guardar')||false,
					dirmedia:obj1.attr('dirmedia')||'',oldmedia:obj1.attr('oldmedia')||'',uploadtmp:obj1.attr('uploadtmp')||false,
					fntmp:obj1.attr('fntmp')||'h',dataurlsave:obj1.attr('dataurlsave')||'media/subir',masvalores:false};

		$.extend(opt,obj);
		var mostraren=__isDom(opt.file)?opt.file:false;
		var acepta='';
		// if(opt.typefile=='imagen'){acepta='image/x-png, image/gif, image/jpeg, image/*'; opt.uploadtmp=true;}
		// else if(opt.typefile=='audio') acepta='audio/mp3';
		// else if(opt.typefile=='video') acepta='video/mp4, video/*';
		// else if(opt.typefile=='html') acepta='.html , .htm , application/zip';
		// else if(opt.typefile=='flash') acepta='.swf , .flv  , application/zip';
		// else if(opt.typefile=='documento') acepta='application/pdf , .doc, .docx, .xls, .xlsx, .ppt, .pptx';
		// else if(opt.typefile=='pdf') acepta='application/pdf , application/zip';
		// else if(opt.typefile=='media') acepta='image/x-png, image/gif, image/jpeg, image/*, audio/mp3, video/mp4, video/*,application/pdf , application/zip,.html , .htm, audio/mp3';
		if(opt.typefile=='imagen'){acepta='image/x-png, image/gif, image/jpeg'; opt.uploadtmp=true;}
		else if(opt.typefile=='enlacesinteres') acepta='text/plain';
		else if(opt.typefile=='audio') acepta='audio/mp3';
		else if(opt.typefile=='video') acepta='video/mp4';
		else if(opt.typefile=='html') acepta='.html , .htm , application/zip';
		else if(opt.typefile=='flash') acepta='.swf , .flv  , application/zip';
		else if(opt.typefile=='documento') acepta='application/pdf , .doc, .docx, .xls, .xlsx, .ppt, .pptx';
		else if(opt.typefile=='pdf') acepta='application/pdf , application/zip';
		else if(opt.typefile=='media') acepta='image/x-png, image/gif, image/jpeg, image/*, audio/mp3, video/mp4, application/pdf, application/zip, .html, .htm, audio/mp3, application/zip';
		else if(opt.typefile=='anymedia') acepta='image/*, audio/mp3, video/mp4, application/pdf, application/zip, .html, .htm, .doc, .docx, .xls, .xlsx, .ppt, .pptx ';
		var file=document.createElement('input');
		file.id='file_'+Date.now();		
		file.type='file';
		file.accept=acepta;
		file.addEventListener('change',function(ev){
			if(opt.uploadtmp && mostraren){
				var rd = new FileReader();
				rd.onload = function(filetmp){
					var filelocal = filetmp.target.result;
					mostraren.attr('src',filelocal);
					var fntm=opt.fntmp;
					if(__isFunction(fntm))fntm(file);
				}
				rd.readAsDataURL(this.files[0]);
			}			
			if(opt.guardar){
				var iduploadtmp='';	
				if(mostraren){
					var $idprogress=$('#clone_progressup').clone(true);
					iduploadtmp='idup'+__idgui();
					$idprogress.attr('id',iduploadtmp);
					//$idprogress.css({'display':'flex'});
					mostraren.closest('div').append($idprogress);
					opt.oldmedia=mostraren.attr('oldmedia')||'';
					iduploadtmp='#'+iduploadtmp;
				}
				
				var data=new FormData()
				data.append('media',this.files[0]);
				data.append('dirmedia',opt.dirmedia);
				data.append('oldmedia',opt.oldmedia||'');
				data.append('typefile',opt.typefile);
				data.append('nombre',opt.nombre||'');
				if(opt.masvalores)
					$.each(opt.masvalores,function(i,v){
						data.append(i,v||'');
					})				
				__sysAyax({ 
					fromdata:data,
		        	url:_sysUrlBase_+opt.dataurlsave,
		        	iduploadtmp:iduploadtmp,
		        	callbackpreload:function(obj,acc){console.log(obj,acc)},
		        	callbackuploadProgress:function(dt){
		        		if(mostraren){

		        			var progress=mostraren.closest('div').children('.progress').children('.progress-bar');
		        			progress.css({width:dt+'%'}).attr('aria-valuenow',dt);
		        			if(dt==100) setTimeout(function(){ mostraren.closest('div').children('.progress').remove() }, 1500);
		        		}
		        	},
		        	callback:function(rs){
		        		if(rs.code==200){
		        			if(rs.media!=''){
		        				if(mostraren){
		        					mostraren.attr('src',rs.media+'?idtmp='+__idgui());
		        					mostraren.attr('data-olmedia',rs.media);
		        				}
		        				if(__isFunction(fn))fn(rs);
		        			} 
		        		}
		        	}
		    	});
			}else{
				if(mostraren){
					var nameimagen=mostraren.attr('alt');
					file.name=nameimagen;
					file.className="input-file-invisible file"+nameimagen;
					file.style='height:0px; width:0px;';
					var hayfile=mostraren.siblings('input[type="file"]');
          			if(hayfile) hayfile.remove();
          			mostraren.parent().append(file);
				}				
			}
		})
		file.click();}
var __sysAyax=function(infodata){
		if(infodata==undefined) return;
		var datasend=infodata.fromdata||'';
		var url=infodata.url||false;
		var type=infodata.type||'json';
		var callback=infodata.callback||false;
		var callbackerror=infodata.callbackerror||false;
		var callbackpreload=infodata.callbackpreload||preload1;
		var callbackuploadProgress=infodata.callbackuploadProgress||false;
		var method=infodata.method||'POST';
		var msjatencion=infodata.msjatencion||'';
		var showmsjok=infodata.showmsjok||false;
		var async=infodata.async||true;
		var obj=infodata.obj||false;
		if(!url) return;
		$.ajax({
	      url: url,
	      type: method,
	      data:  datasend,
	      contentType: false,
	      processData: false,
	      cache: false,
	      dataType:type,
	      async:async,
	      beforeSend: function(XMLHttpRequest){if(__isFunction(callbackpreload))callbackpreload(obj,true);},
	      xhr: function(){
		        var xhr = new window.XMLHttpRequest();
		        xhr.upload.addEventListener("progress", function(evt) {
		            //if (evt.lengthComputable) {
		                var percentComplete = (evt.loaded / evt.total) * 100;
		                if(__isFunction(callbackuploadProgress))
	      					callbackuploadProgress(percentComplete)
		                //Do something with upload progress here
		            //}
		       }, false);
		       return xhr;
		  },
	      /*uploadProgress:function(event, position, total, percentComplete){
	      	if(__isFunction(callbackuploadProgress))
	      		callbackuploadProgress({obj,event, position, total, percentComplete})
	      },*/ 
	      success: function(data)
	      { 
	        if(type=='json'){
		        if(data.code=='Error'||data.code=='error'){
		          if(__isFunction(callbackpreload))callbackpreload(obj,false);
		          __notificar({title:msjatencion,html:data.msj,type:'warning'});
		          if(__isFunction(callbackerror))callback(data);
		        }else{
		          if(showmsjok) __notificar({title:msjatencion,html:data.msj,type:'success'});
		          if(__isFunction(callback))callback(data);
		        }
		    }else{
		    	if(__isFunction(callback))callback(data);
		    }
	      },
	      error: function(e){ console.log('error',e,infodata); if(__isFunction(callbackpreload))callbackpreload(obj,false);},
	      complete: function(xhr){ if(__isFunction(callbackpreload))callbackpreload(obj,false); }
	    });
}

var mostrar_notificacion=function(msjatencion,msj,type){
	__notificar({title:msjatencion,html:msj,type:type||'success'});
}
var __notificar=function(datos){
	var option={
		type:'info',
		title:'',
		animation:true,
		text:'',		
		html: '',
		showCloseButton: false,
		showCancelButton: false,
		focusConfirm: false,
		confirmButtonText:'Aceptar',
	    confirmButtonAriaLabel: 'Aceptar',
	    cancelButtonText:'Cancelar',
	    cancelButtonAriaLabel: 'Cancelar',
	    //timer: 1500,
	    //background: '#fff url(/images/trees.png)',
		backdrop: `
		    rgba(0,0,123,0.4)		    
		    center left
		    no-repeat
		`,
		onBeforeOpen:'',
		onClose:'',
		/*customClass: {			
		    popup: 'animated tada'
		}*/
	}
	var opt=__extend(option,datos||{});
	if(datos.callback==undefined)	Swal.fire(opt);
	else{
		Swal.fire(opt).then(r=>{
			if(__isFunction(datos.callback)) datos.callback(r);
		})
	}
}

var __initEditor=function(tmpid,mplugins){
	var mplugins=mplugins||'';	
	tinymce.init({
	  	relative_urls : false,
	    remove_script_host : false,
	    convert_urls : true,
	    convert_newlines_to_brs : true,
	    menubar: false,
	    //statusbar: false,
	    verify_html : false,
	    content_css : [_sysUrlBase_+'/static/tema/css/bootstrap.min.css'],
	    selector: '#'+tmpid,
	    height: 210,
	    paste_auto_cleanup_on_paste : true,
	    paste_preprocess : function(pl, o) {
	        var html='<div>'+o.content+'</div>';
	        var txt =$(html).text();
	        o.content = txt;
	        },
	    paste_postprocess : function(pl, o) {
	        o.node.innerHTML = o.node.innerHTML;
	        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
	        },
	    plugins:[mplugins+" textcolor paste lists advlist" ],  // chingoinput chingoimage chingoaudio chingovideo
	    toolbar: ' | undo redo '+mplugins+' | styleselect | removeformat |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect| ', //chingoinput chingoimage chingoaudio chingovideo,
	    advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",	   
	});
}


var __initNewEditor=function(tmpid,mplugins){
	var mplugins=mplugins||'';	
	tinymce.init({
	  	relative_urls : false,
	    remove_script_host : false,
	    convert_urls : true,
	    convert_newlines_to_brs : true,
	    menubar: false,
	    //statusbar: false,
	    verify_html : false,
	    content_css : [_sysUrlBase_+'/static/tema/css/bootstrap.min.css'],
	    selector: '#'+tmpid,
	    height: 210,
	    paste_auto_cleanup_on_paste : true,
	    paste_preprocess : function(pl, o) {
	        var html='<div>'+o.content+'</div>';
	        var txt =$(html).text();
	        o.content = txt;
	        },
	    paste_postprocess : function(pl, o) {
	        o.node.innerHTML = o.node.innerHTML;
	        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
	        },
	    plugins:[mplugins+" textcolor paste lists advlist" ],  // chingoinput chingoimage chingoaudio chingovideo
	    toolbar: ' | undo redo '+mplugins+' | styleselect | removeformat |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect| ', //chingoinput chingoimage chingoaudio chingovideo,
	    advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
	    setup: function (editor){
	        editor.on('change', function () {
            	tinymce.triggerSave();
        	});
    	}
	});
}

var preload1=function(_this,acc){
	if(_this.length){
		if(acc==true){
			_this.addClass('disabled').append(' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
		}else{
			_this.removeClass('disabled').children('.spinner-border').remove();
		}
	}
}
var modal_id = 0;
var __stopmedios=function(c){
	if(c.length){
		var a=$(c.find('audio')); if(a.length) a.trigger('pause');
	    var v=$(c.find('video')); if(v.length) v.trigger('pause');
	}
	return true;
}
var __cerrarmodal=function(_md,sb){
	if(_md.length){
    	__stopmedios(_md);
		_md.modal('hide');
		//console.log(sb);
		if(sb)_md.on('hidden.bs.modal', function(){_md.remove();});		
	}		
}
var __sysmodal=function(infodata){
		var dt=infodata||{};
		var sh=dt.header||false;
		var sf=dt.footer||false;
		var sb=dt.borrar||true;
		var tm=dt.tam||'lg';
		var mw=dt.modalancho||'';
		var cl=dt.clase||'';
		var ur=dt.url||'';
		var ht=dt.html||'';
		var mt=dt.method||'POST';
		var fd=dt.fromdata||'';
		var bd=dt.backdrop||'static';
		var kb=dt.keyboard||true;
		var ti=dt.titulo||'';
		var fu=dt.func||'';	
		var zi=dt.zindex||false;
		var _md = $('#modalclone').clone(true);
		if(!sh&&ti=='') _md.find('.modal-header').hide();
		if(!sf) _md.find('.modal-footer').hide();
		_md.attr('id', 'modal-' + modal_id);
	    _md.addClass(cl);
	    _md.children('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tm)
	    if(mw!='')_md.children('.modal-dialog').css('width',mw);
	    $('body').on('click', '#modal-' + modal_id + ' .close , #modal-' + modal_id + ' .btn-close, #modal-' + modal_id + ' .cerrarmodal' , function (ev){ 
	    	ev.preventDefault(); ev.stopPropagation(); __cerrarmodal(_md , sb)});
	   	//$('body').on('click', '' , function (ev){ __cerrarmodal(_md , sb)});
	   	// $('body').on('click', ' , function (ev){ __cerrarmodal(_md , sb)});
		//  $('body').on('hide.bs.modal', '#modal-' + modal_id , function (ev){ __cerrarmodal(_md , sb)});
	    
	    _md.find('#modaltitle').html(ti);
	    if(ur!=''){
	    	__sysAyax({
	    		url:ur,
	    		method:mt,
	    		fromdata:fd,
	    		processData: false,
	    		async:false,
	    		type:'html',    		
	    		callback:function(rs){
					_md.find('#modalcontent').html(rs);
					_md.modal({backdrop: bd, keyboard: kb});
					_md.on('cerramodal', function (ev){ __cerrarmodal(_md , sb)});
					if(__isFunction(fu))fu();
	    		}
	    	});
		}else if(ht!=''){
			_md.find('#modalcontent').html(ht);
			_md.modal({backdrop: bd, keyboard: kb}); 
			_md.on('cerramodal', function (ev){  __cerrarmodal(_md , sb)});
		}

		_md.on('shown.bs.modal',function(){
			//console.log(zi);
			if(zi==false){
				_md.css({'z-index':1040+modal_id*10});
				_md.prev('.modal-backdrop').css({'z-index':1040+(modal_id*10)-2});
			}
			//console.log(_md.prev('.modal-backdrop'));
		})
		++modal_id;
	    return _md;
	}

var MSJES_PHP = {};
function cargarMensajesPHP(idContenedor) {
	var $sectionMensajes = $(idContenedor);
	$sectionMensajes.find('input').each(function() {
		var id = $(this).attr('id');
		var valor = $(this).val();
		MSJES_PHP[id] = valor;
	});
};
$(document).ready(function(){
    var altoventana=$(window).height();
	if(isMobile.any()||altoventana>700){
		$('header').addClass('static');
	}
    $(window).resize(function(){
      return resizemodal($(".modal"));
    });
   
    /*$(document).mousemove(function(event){
    	var header= $('header');
    	if(header.hasClass('static')){
    		return false;
    	}
        header.addClass('isshow');
        var toolbar=$('aside');
        //var anchopantalla=$(window).width()-100;
        if(event.pageY<=3){
            if(!header.hasClass('sysshow')&&header.hasClass('ishide')){
               header.removeClass('ishide').slideDown('fast');
            }
        }else{
        	var submeduactive= $('.menutop1 li.dropdown.open');
            if(!toolbar.hasClass('toolbar-toggled')&&event.pageY>100&&submeduactive.length==0){ //&&event.pageX>anchopantalla;
                if(header.length)
                	header.addClass('ishide').removeClass('sysshow').slideUp('fast');
            }
        }
    });
    *//*
    	$(document).on('click','.panel-heading span.clickable', function(e){
	  		var $this = $(this);
		if(!$this.hasClass('panel-collapsed')){
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		}else{
			$this.parents('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});*/

	$('.changeidioma').click(function(){
        var idi=$(this).attr('idioma');
        localStorage.setItem("sysidioma", idi);
        xajax__('', 'idioma', 'cambiaridioma', idi);        
    });
    $('.changerol').click(function(){
        xajax__('', 'Sesion', 'cambiarRol');
    });

    /*__imageToDataURL=function(url){
    	//var img=new Image();
    	var img = document.createElement('img');
    	img.src = url;
    	return img;
    }*/

	__cambiarinfouser=function(){
		try{
	    	var infouser=JSON.parse(localStorage.getItem('usersesion'));
	    	var varfoto=infouser.foto||'static/media/usuarios/user_avatar.jpg';
	        var ifoto=varfoto.lastIndexOf("static/");
	        if(ifoto>=0) varfoto=_sysUrlBase_+'/'+varfoto.substring(ifoto);
	        if(ifoto==-1){
	          var ifoto=varfoto.lastIndexOf("/");
	          var ifoto2=varfoto.lastIndexOf(".");
	          if(ifoto>=0 && ifoto2>=0) varfoto=_sysUrlBase_+'/static/media/usuarios/'+varfoto.substring(ifoto);
	          else if(ifoto2>=0) varfoto=_sysUrlBase_+'/static/media/usuarios/'+varfoto;
	          else varfoto=_sysUrlBase_+'/static/media/usuarios/user_avatar.jpg';
	        }
	     	$('.user-foto').attr('src',varfoto);
	    	$('.user-nombre').html(infouser.nombre_full);
	    	$('.user-rol').html(infouser.rol);
	    	$('.user-email').html(infouser.email);
	    	$('.user-tel').html(infouser.tel);
	    	return infouser;
    	}catch(error){
  			console.error(error);
  		}
    }
    
    __infoempresa=function(id){
		var json = localStorage.getItem('userempresa'); 
		var emp=json != null ? JSON.parse(localStorage.getItem('userempresa')) : {};
		if(Object.keys(emp).length > 0 && emp.tipo_empresa==0){ // ocultar controles dre/ugel
			if(id!=undefined)
			if(id.length) var ele=id.find('.tipoempresa1'); 
			var ele=$('.tipoempresa1');
			$.each(ele,function(i,v){
	    		if($(v).find('select').length) $(v).hide(0);
	    		else $(v).remove();
			})
		}
		return emp;
	}

    __logoEmpresaimagebase64=function(){
    	var _infoempresa=__infoempresa();
    	if(Object.keys(_infoempresa).length == 0){
    		return false;
    	}
    	var logoempresa=_infoempresa.logoempresa;
        var img = document.createElement('img');
        img.addEventListener( 'load',function (event){
            _img=event.currentTarget;
            var canvas = document.createElement('canvas');
            canvas.width = _img.width;
            canvas.height = _img.height;
                var ctx = canvas.getContext('2d');
                ctx.drawImage(_img, 0, 0 ); 
                _logobase64=canvas.toDataURL();
        })
        if(logoempresa!='') img.src = _sysUrlBase_+logoempresa;
    }


});