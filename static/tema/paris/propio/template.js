function redir_(url) {
    location.href = url;
}

function addItemNavBar(elemento) {
    var html = '',
        c = "'";
    if (elemento.link === undefined) {
        elemento.link = '';
    } else {
        elemento.link = ' onclick="location.href=' + c + elemento.link + c + '"';
    }
    if ($(".paris-navbar-container").html() == "") {
        html += '<div class="col-md-12 col-sm-12">';
        html += '    <div class="x_panel paris-navbar-panel">';
        html += '      <div class="x_content paris-navbar">';
        html += '        <div class="col-md-8 paris-left">';
        html += '           <button type="button" class="paris-item btn btn-default btn-sm" onclick="history.back()"><i class="fa fa-reply"></i> ' + navBarParisReturnText + '</button>';
        html += '          <label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>';
        html += '               <button type="button" class="paris-item btn btn-default btn-sm"' + elemento.link + '>' + elemento.text + '</button>';
        html += '        </div>';
        html += '        <div class="col-md-4 paris-right"></div>';
        html += '      </div>';
        html += '    </div>';
        html += '  </div>';
        $(".paris-navbar-container").addClass("row").html(html);
    } else {
        html += '<label class="paris-divisor"><i class="fa fa-chevron-right"></i></label>';
        html += '<button type="button" class="paris-item btn btn-default btn-sm"' + elemento.link + '>' + elemento.text + '</button>';
        $(".paris-left").append(html);
    }
}

function addButonNavBar(elemento) {
    var html = '',
        c = "'";
    if (elemento.id === undefined) {
        elemento.id = '';
    } else {
        elemento.id = ' id="' + elemento.id + '"';
    }
    if (elemento.data === undefined) {
        elemento.data = {};
    }
    if (elemento.class === undefined) {
        elemento.class = ' class="paris-item btn btn-default"';
    } else {
        elemento.class = ' class="paris-item ' + elemento.class + '"';
    }
    if (elemento.link === undefined) {
        elemento.link = '';
    } else {
        elemento.link = 'location.href=' + c + elemento.link + c + ';';
    }
    if (elemento.click === undefined) {
        elemento.click = '';
    }
    if (elemento.href === undefined) {
        elemento.href = '';
    } else {
        elemento.href = ' href="' + elemento.href + '"';
    }
    if (elemento.attr === undefined) {
        elemento.attr = {};
    }
    if ($(".paris-navbar-container").html() == "") {
        html += '<div class="col-md-12 col-sm-12 ">';
        html += '    <div class="x_panel">';
        html += '      <div class="x_content paris-navbar">';
        html += '        <div class="col-md-8 paris-left">';
        html += '           <button type="button" class="paris-item btn btn-default btn-sm" onclick="history.back()"><i class="fa fa-reply"></i> ' + navBarParisReturnText + '</button>';
        html += '        </div>';
        html += '        <div class="col-md-4 paris-right">';
        html += '           <button type="button"' + elemento.id + elemento.class + ' onclick="' + elemento.link + elemento.click + '"' + elemento.href;
        for (let key of Object.keys(elemento.data)) {
            html += ' data-' + key + '="' + elemento.data[key] + '"';
        }
        for (let key of Object.keys(elemento.attr)) {
            html += ' ' + key + '="' + elemento.attr[key] + '"';
        }
        html += '           >' + elemento.text + '</button>';
        html += '        </div>';
        html += '      </div>';
        html += '    </div>';
        html += '  </div>';
        $(".paris-navbar-container").addClass("row").html(html);
    } else {
        html += '<button type="button"' + elemento.id + elemento.class + ' onclick="' + elemento.link + elemento.click + '"' + elemento.href;
        for (let key of Object.keys(elemento.data)) {
            html += ' data-' + key + '="' + elemento.data[key] + '"';
        }
        for (let key of Object.keys(elemento.attr)) {
            html += ' ' + key + '="' + elemento.attr[key] + '"';
        }
        html += '>' + elemento.text + '</button>';
        $(".paris-right").append(html);
    }
}

function addWidgetPanel(elemento) {
    var html = '',
        c = "'";
    if (elemento.link === undefined) {
        elemento.link = '';
    } else {
        elemento.link = ' onclick="redir_(' + c + elemento.link + c + ')"';
    }
    if (elemento.ribbon === undefined) {
        elemento.ribbon = '';
    } else {
        elemento.ribbon = ' ui-ribbon-container';
        elemento.link = '';
    }
    if (elemento.icono === undefined) {
        elemento.icono = '';
    } else {
        elemento.icono = ' class="fa ' + elemento.icono + '"';
    }
    if (elemento.slickItem === undefined) {
        elemento.slickItem = '';
    } else {
        elemento.slickItem = ' slick-item';
    }
    if (elemento.classTipo === undefined) {
        elemento.classTipo = '';
    } else {
        elemento.classTipo = ' ' + elemento.classTipo;
    }
    html += '<div class="col-md-4 widget_tally_box' + elemento.slickItem + elemento.classTipo + '" ' + elemento.link + ' title="' + elemento.titulo + '">';
    html += '	<div class="x_panel panel_mnu' + elemento.ribbon + '">';
    html += '		<div class="text-center">';
    html += '			<h1><i' + elemento.icono + '></i></h1>';
    html += '			<h4><strong>' + elemento.titulo + '</strong></h4>';
    html += '		</div>';
    html += '	</div>';
    html += '</div>';
    if (elemento.contentClass !== undefined) {
        $('.' + elemento.contentClass).append(html);
    } else if (elemento.contentId !== undefined) {
        $('#' + elemento.contentId).append(html);
    }
}