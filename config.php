<?php
/*if(!isset($_SESSION)){
    @session_start(); 
}*/
date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
ini_set('display_errors', '1');


define('CARPETA_RAIZ', 'smartcourse/');

//define('CARPETA_RAIZ', '');
define('CARPETA_SMARTQUIZ', 'smartquiz/');
define('_version_', '1111');
define('IDPROYECTO', 'smartcourse');
define('IDLEARN', 'loadtest');
define('DOMINIO_SKS', 'localhost/smart/smartsks');
define('_ishttp_', true);
define('MODODEVELOP', true);

/********************************************
//parametros developer testing email
********************************************/
//definir si tomara o no el correo configurado de la empresa para enviar los correos
//false=si tomara datos de correo de empresa
//true = no tomara datos de la empresa
//esta variable es global pero se podra definir a nivel de modulo tambien 
define('MODODEVELOP_emailempresa',true); 

//configurar lista de correos a donde se pueden hacer pruebas.
define('email_simulardestinatarios', true);
//lista de correo, se acompaña  con la variable email_simulardestinatarios que debe estar en true;
define('email_destinatarios_dev',json_encode(
    array(
        array('email' =>'abelchingo@gmail.com','nombre'=>'ññÑáéíóúâêîÂáÁ É'),
        array('email'=>'abel_chingo@hotmail.com','nombre'=>'Abel ññÑáéíóúâêîÂáÁ É'),
       // array('email'=>'aldo.davila@gmail.com','nombre'=>'Aldo âáA'),        
        //array('email'=>'willy_7873@hotmail.com','nombre'=>'Willy âáA'),
       // array('email'=>'aldo.davila@smartknowledge-solutions.com','nombre'=>'Aldo âáA'),
        //array('email' =>'emmyseco@gmail.com', 'nombre'=>'Emmy âáA')
    )
));


//solo enviara correo a abelchingo@gmail.com

define('SENCE_TEST', true);
define(
    "_CONFIG_",
    json_encode(
        array(
            "familia" => array(
                "tema" => "familia", "is_login" => false
            ), "frontend" => array(
                "tema" => "paris", "is_login" => true
            ), "json" => array(
                "tema" => "api", "is_login" => false
            ), "proyecto" => array(
                "tema" => "paris", "is_login" => true
            ), "smart" => array(
                "tema" => "paris", "is_login" => true
            ), "quiz" => array(
                "tema" => "quiz", "is_login" => true
            ), "smartbook" => array(
                "tema" => "smartbook", "is_login" => true
            )
        )
    )
);

// servidor local
  define("HOST_BD", "localhost");
  define("USER_BD", "root");
  define("PWD_BD", 'root');
  define("NAME_BD", "sc");
  define("NAME_BDQUIZ", "squiz");
  define("NAME_BDSEAdultos", "se_adultos");
  define("NAME_BDSEAdolecentes", "se_adolescentes");
// define("URL_SEAdultos", "https://seadultos.skscourses.local/");
// define("URL_SEAdolecentes", "https://seadolescentes.skscourses.local/");

define("URL_SEAdultos", "https://junior.desarrollador.ml/");
define("URL_SEAdolecentes", "https://junior.desarrollador.ml/");


/* No cambiar */
$host = !empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : 'localhost';
$url = @$_SERVER["REQUEST_URI"];
if (defined('_http_') == false) define('_http_', !empty($_SERVER['HTTPS']) ? 'https://' : 'http://');
if (defined('SD') == false) define('SD', DIRECTORY_SEPARATOR);
if (defined('_HOST_') == false) define('_HOST_', _http_ . $host);
if (defined('_URL_SKS_') == false) define('_URL_SKS_', _http_ . DOMINIO_SKS);
define('_BOOT_', true);
$ruta_ini = dirname(__FILE__);
define('RUTA_BASE', $ruta_ini . SD);
define('RUTA_RAIZ', RUTA_BASE);
define('RUTA_LIBS', RUTA_RAIZ . 'sys_lib' . SD);
define('RUTA_INC',  RUTA_BASE . 'sys_inc' . SD);
define('URL_BASE', _HOST_ . '/' . CARPETA_RAIZ);
define('URL_RAIZ', URL_BASE);
define('URL_MEDIA', URL_RAIZ);
define('URL_SMARTQUIZ', URL_BASE . CARPETA_SMARTQUIZ);


/************* producción ************/
//Ruta de SE Adolescentes y adultos 
//define('RUTA_BASE_SEJ',dirname(dirname(__FILE__)).SD.'english_juniors'.SD.'web'.SD);
//define('RUTA_BASE_SEA',dirname(dirname(__FILE__)).SD.'english_adultos'.SD);

/************** desarrollo *********/
define('RUTA_BASE_SEA',dirname(dirname(__FILE__)).SD.'english_adultos'.SD);
define('RUTA_BASE_SEJ',RUTA_BASE_SEA);

/*$_SESSION["urlredirok"] = _http_ . $host . $url;*/
/*if (_ishttp_ == true && empty($_SERVER['HTTPS'])) {
    header('Location:https://' . $host . $url);
    exit(0);
}*/
