ALTER TABLE `acad_grupoaula` ADD `idgrado` INT NULL DEFAULT '0' AFTER `oldid`, ADD `idsesion` INT NULL DEFAULT '0' AFTER `idgrado`;
ALTER TABLE `acad_matricula` ADD `fecha_vencimiento` DATETIME NULL DEFAULT NULL AFTER `oldid`;
ALTER TABLE `personal` ADD `esdemo` TINYINT(2) NOT NULL DEFAULT '0';
ALTER TABLE `proyecto` ADD COLUMN `nombre` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `proyecto` ADD COLUMN `tipo_empresa` TINYINT(4) NULL DEFAULT '1' COMMENT '1: mostrar dre, 0 ocultar' ;
ALTER TABLE `proyecto` ADD COLUMN `logo` VARCHAR(255) NULL DEFAULT NULL ;
ALTER TABLE `proyecto` ADD COLUMN `nombreurl` VARCHAR(60) NULL DEFAULT NULL COMMENT 'EMP_edumax' ;
ALTER TABLE `bolsa_empresas` DROP `tipo_empresa`,  DROP `nombreurl`;
ALTER TABLE `notas_quiz` ADD `preguntasoffline` TEXT NULL DEFAULT NULL AFTER `preguntas`;


